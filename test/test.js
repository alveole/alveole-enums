/* eslint-disable */
const expect = require('chai').expect;
const enums = require('../enums');

const checkSchema = ([keyName, type], value) => {
    if (typeof type === 'object') {
        return Object.entries(type)
            .forEach(([subKeyName, subType]) =>
                checkSchema([subKeyName, subType], value[subKeyName]));
    }
    if (type.includes(typeof value)) return true;
    throw `schema mismatch. Expected  key of ${keyName} with value ${value} to be type ${type} and not ${typeof value}`;
};

describe('require(\'./enums\')', function() {
    it('should be a factory function', function() {
        expect(enums).to.be.a('function');
    });

    it('should return an object', function() {
        expect(enums()).to.be.an('object');
    });
});

describe('enums.regions', function() {
    it('should be a function', function() {
        expect(enums().regions).to.be.a('function');
    });

    it('should return an object with all and get', function() {
        const regions = enums().regions();
        expect(regions).to.be.an('object');
        expect(regions.all).to.be.a('function');
        expect(regions.get).to.be.a('function');
    });
});

describe('enums.regions.all', function() {
    it('should be a function', function() {
        expect(enums().regions().all).to.be.a('function');
    });

    it('should return an object with a number of keys', function() {
        const allRegions = enums().regions().all();
        expect(Object.keys(allRegions).length).to.be.greaterThan(1)
    });
});

describe('enums.regions.get', function() {
    it('should be a function', function() {
        expect(enums().regions().get).to.be.a('function');
    });

    it('should return region objects', function() {
        for (let i = 0; i < 7; i += 1) {
            expect(enums().regions().get(i)).to.be.an('object');
        }
    });
    
    it('should return this matching of regions', function() {
        expect(enums().regions().get(0).name.fr).to.equal('Montréal');
        expect(enums().regions().get(1).name.fr).to.equal('Québec');
        expect(enums().regions().get(2).name.fr).to.equal('Toronto');
        expect(enums().regions().get(3).name.fr).to.equal('Ottawa');
        expect(enums().regions().get(4).name.fr).to.equal('Calgary');
        expect(enums().regions().get(5).name.fr).to.equal('Vancouver');
        expect(enums().regions().get(39).name.fr).to.equal('Victoria');
    });

    it('should return regions that follow this schema', function () {
        const schema = {
            "emails": "string",
            "greaterRegion": {
                "en": "string",
                "fr": "string"
            },
            "initials": "string",
            "location": {
                "lat": "number object", // object cause null 
                "lng": "number object" // object cause null 
            },
            "name": {
                "en": "string",
                "fr": "string"
            },
            "time": {
                "locale": "string",
                "longFormat": "string",
                "shortFormat": "string",
                "zone": "string",
            },
        }
        expect(Object.entries(enums().regions().all()).map(([key, region]) => checkSchema([null, schema], region))).to.be.an('array');
    });

    it('should be an object on a bad number input', function() {
        for (let i = 0; i < 10000; i += 1) {
            expect(enums().regions().get(Math.ceil(Math.random(i) * 1000))).to.be.an('object');
        }
    });

    it('should return "Unknown region" when it does not know the region', function() {
        expect(enums().regions().get(129873193)).to.be.an('object');
        expect(enums().regions().get(129873193).name.en).to.equal('Unknown region');
    });

    it('should expect region to be a number or string', function() {
        const error = 'Expected code to be a string or number.';
        const _enums = enums();
        expect(_enums.regions().get.bind(_enums, {})).to.throw(error);
        expect(_enums.regions().get.bind(_enums, undefined)).to.throw(error);
        expect(_enums.regions().get.bind(_enums, null)).to.throw(error);
        expect(_enums.regions().get.bind(_enums, false)).to.throw(error);
    });
});

describe('enums.region', function() {
    it('should be a function', function() {
        expect(enums().region).to.be.a('function');
    });

    it('should return region objects', function() {
        for (let i = 0; i < 7; i += 1) {
            expect(enums().region(i)).to.be.an('object');
        }
    });
    
    it('should return this matching of regions', function() {
        expect(enums().region(0).name.fr).to.equal('Montréal');
        expect(enums().region(1).name.fr).to.equal('Québec');
        expect(enums().region(2).name.fr).to.equal('Toronto');
        expect(enums().region(3).name.fr).to.equal('Ottawa');
        expect(enums().region(4).name.fr).to.equal('Calgary');
        expect(enums().region(5).name.fr).to.equal('Vancouver');
        expect(enums().region(6).name.fr).to.equal('Chicago');
    });

    it('should be an object on a bad number input', function() {
        for (let i = 0; i < 10000; i += 1) {
            expect(enums().region(Math.ceil(Math.random(i) * 1000))).to.be.an('object');
        }
    });

    it('should return "Unknown region" when it does not know the region', function() {
        expect(enums().regions().get(129873193)).to.be.an('object');
        expect(enums().regions().get(129873193).name.en).to.equal('Unknown region');
    });

    it('should expect region to be a string or number', function() {
        const error = 'Expected code to be a string or number.';
        const _enums = enums();
        expect(_enums.regions().get.bind(_enums, {})).to.throw(error);
        expect(_enums.regions().get.bind(_enums, undefined)).to.throw(error);
        expect(_enums.regions().get.bind(_enums, null)).to.throw(error);
        expect(_enums.regions().get.bind(_enums, false)).to.throw(error);
    });
});

describe('enums.clientRegion', function() {
    const _enums = enums();
    it('should expect client to be an object', function() {
        const error = 'Expected client to be an object.';
        expect(_enums.clientRegion.bind(_enums, 'garbage')).to.throw(error);
        expect(_enums.clientRegion.bind(_enums, undefined)).to.throw(error);
        expect(_enums.clientRegion.bind(_enums, null)).to.throw(error);
        expect(_enums.clientRegion.bind(_enums, false)).to.throw(error);
    });

    it('should expect client.get to be a function', function() {
        expect(_enums.clientRegion.bind(_enums, {})).to.throw('Expected client to implement get.');
    });
});

describe('enums().statusFromRdv', function() {
    const _enums = enums();

    it('should be a function', function() {
        expect(_enums.statusFromRdv).to.be.a('function');
    });

    it('should expect rdv to be an object', function() {
        const error = 'Expected RDV to be an object.';
        expect(_enums.statusFromRdv.bind(_enums, 'garbage')).to.throw(error);
        expect(_enums.statusFromRdv.bind(_enums, undefined)).to.throw(error);
        expect(_enums.statusFromRdv.bind(_enums, null)).to.throw(error);
        expect(_enums.statusFromRdv.bind(_enums, false)).to.throw(error);
    });
});

describe('enums().rdvStatuses', function() {
    it('should be a function', function() {
        expect(enums().rdvStatuses).to.be.a('function');
        expect(enums().rdvStatuses()).to.be.an('object');
        expect(enums().rdvStatuses().get).to.be.a('function');
    });
});

describe('enums().rdvStatuses().all', function() {
    it('should be a function', function() {
        expect(enums().rdvStatuses().all).to.be.a('function');
    });

    it('should return all of the rdv statuses', function() {
        const statuses = enums().rdvStatuses().all();
        expect(statuses).to.be.an('object');
        expect(statuses).to.deep.equal({
            0: {
                name: {
                    en: 'Unconfirmed',
                    fr: 'Non confirmé',
                },
                color: 'red',
            },
            1: {
                name: {
                    en: 'Waiting',
                    fr: 'En attente',
                },
                color: 'blue',
            },
            2: {
                name: {
                    en: 'Client contacted',
                    fr: 'Client contacté',
                },
                color: 'orange',
            },
            3: {
                name: {
                    en: 'Confirmed',
                    fr: 'Confirmé',
                },
                color: 'green',
            },
            4: {
                name: {
                    en: 'Cancelled',
                    fr: 'Annulée',
                },
                color: 'black',
            },
            5: {
                name: {
                    en: 'Deleted',
                    fr: 'Supprimée',
                },
                color: 'black',
            },
            10: {
                name: {
                    en: 'Placed automatically',
                    fr: 'Placé automatiquement',
                },
                color: 'purple',
            }
        })
    });
});


describe('enums().visitTypes().all()', function () {
    it('the workshops should be a function', function () {
        // console.log(enums().visitTypes().all())
        expect(enums().visitTypes().all).to.be.a('function');
    });
    it('should return an object', function () {
        expect(enums().visitTypes().all()).to.be.an('object');
    });
    it('should follow the schema', function () {
        expect(enums().visitTypes().all()).to.be.an('object');
    const schema = {
        id: "string",
        name: {
            en: "string",
            fr: "string"
        },
        recommendedDateRanges: {
            '0': "object", // array
            '1': "object", // array
            '2': "object", // array
            '3': "object", // array
            '4': "object", // array
            '5': "object", // array
            '6': "object", // array
            '7': "object", // array
            '8': "object", // array
            '9': "object", // array
            '10': "object", // array
            '11': "object", // array
            '12': "object", // array
            '13': "object", // array
            '14': "object", // array
            '15': "object", // array
            '16': "object", // array
            '17': "object", // array
            '18': "object", // array
            '19': "object", // array
            '20': "object", // array
            '21': "object", // array
            '22': "object", // array
            '23': "object", // array
            '24': "object", // array
            '25': "object", // array
            '26': "object", // array
            '27': "object", // array
            '28': "object", // array
            '29': "object", // array
            '30': "object", // array
            '31': "object", // array
            '32': "object", // array
            '33': "object", // array
            '34': "object", // array
            '35': "object", // array
            '36': "object", // array
            '37': "object", // array
            '38': "object", // array
            '39': "object"
        },
        tags: "object",
        timings: { setup: "number", show: "number", cleanup: "number", total: "number" },
        inPerson: "boolean"
    }
    const workshops = Object.entries(enums().visitTypes().all()).filter(([key, value]) => key.startsWith('ate_')).map(([key, val]) => val);
    expect(workshops.map((workshop) => checkSchema([null, schema], workshop))).to.be.an('array');
    });
});
