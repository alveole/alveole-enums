/* eslint-disable */
(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
        typeof define === 'function' && define.amd ? define(factory) :
        global.alveoleEnums = factory();
}(this, (function () { 'use strict';

    const enums = {
        regions: {
            0: {
                name: {
                    en: 'Montreal',
                    fr: 'Montréal',
                },
                initials: 'MTL',
                emails: 'montreal@alveole.buzz',
                time: { 
                    zone: 'America/Toronto',
                    locale: 'fr',
                    longFormat: 'dddd MMMM D YYYY H:mm',
                    shortFormat: 'H:mm',
                },
                location: {
                    lat: 45.537880,
                    lng: -73.640055,
                },
                greaterRegion: {
                    en: "Canada",
                    fr: "Canada"
                },
            },
            1: {
                name: {
                    en: 'Quebec',
                    fr: 'Québec',
                },
                initials: 'QC',
                emails: 'quebec@alveole.buzz',
                time: { 
                    zone: 'America/Toronto',
                    locale: 'fr',
                    longFormat: 'dddd MMMM D YYYY H:mm',
                    shortFormat: 'H:mm',
                },
                location: {
                    lat: 46.810217,
                    lng: -71.232966,
                },
                greaterRegion: {
                    en: "Canada",
                    fr: "Canada"
                },
            },
            2: {
                name: {
                    en: 'Toronto',
                    fr: 'Toronto',
                },
                initials: 'TO',
                emails: 'toronto@alveole.buzz',
                time: { 
                    zone: 'America/Toronto',
                    locale: 'en',
                    longFormat: 'dddd MMMM D YYYY H:mm',
                    shortFormat: 'H:mm',
                },
                location: {
                    lat: 43.669139,
                    lng: -79.464748,
                },
                greaterRegion: {
                    en: "Canada",
                    fr: "Canada"
                },
            },
            3: {
                name: {
                    en: 'Ottawa',
                    fr: 'Ottawa',
                },
                initials: 'OTT',
                emails: 'ottawa@alveole.buzz',
                time: { 
                    zone: 'America/Toronto',
                    locale: 'en',
                    longFormat: 'dddd MMMM D YYYY H:mm',
                    shortFormat: 'H:mm',
                },
                location: {
                    lat: 45.410667,
                    lng: -75.626387,
                },
                greaterRegion: {
                    en: "Canada",
                    fr: "Canada"
                },
            },
            4: {
                name: {
                    en: 'Calgary',
                    fr: 'Calgary',
                },
                initials: 'CGY',
                emails: 'calgary@alveole.buzz',
                time: { 
                    zone: 'America/Edmonton',
                    locale: 'en',
                    longFormat: 'dddd MMMM D YYYY H:mm',
                    shortFormat: 'H:mm',
                },
                location: {
                    lat: 51.0168975, 
                    lng: -114.0207094,
                },
                greaterRegion: {
                    en: "Canada",
                    fr: "Canada"
                },
            },
            5: {
                name: {
                    en: 'Vancouver',
                    fr: 'Vancouver',
                },
                initials: 'VAN',
                emails: 'vancouver@alveole.buzz',
                time: { 
                    zone: 'America/Vancouver',
                    locale: 'en',
                    longFormat: 'dddd MMMM D YYYY H:mm',
                    shortFormat: 'H:mm',
                },
                location: {
                    lat: 49.221300, 
                    lng: -122.985530,
                },
                greaterRegion: {
                    en: "Canada",
                    fr: "Canada"
                },
            },
            6: {
                name: {
                    en: 'Chicago',
                    fr: 'Chicago',
                },
                initials: 'CHI',
                emails: 'chicago@alveole.buzz',
                time: { 
                    zone: 'America/Chicago',
                    locale: 'en',
                    longFormat: 'dddd MMMM D YYYY H:mm',
                    shortFormat: 'H:mm',
                },
                location: {
                    lat: 41.890446, 
                    lng: -87.686127,
                },
                greaterRegion: {
                    en: "United States",
                    fr: "États-Unis"
                },
            },
            7: {
                name: {
                    en: 'New York City',
                    fr: 'New York',
                },
                initials: 'NYC',
                emails: 'newyork@alveole.buzz',
                time: { 
                    zone: 'America/Toronto',
                    locale: 'en',
                    longFormat: 'dddd MMMM D YYYY H:mm',
                    shortFormat: 'H:mm',
                },
                location: { // TODO
                    lat: null, 
                    lng: null,
                },
                greaterRegion: {
                    en: "United States",
                    fr: "États-Unis"
                },
            },
            8: {
                name: {
                    en: 'Philadelphia',
                    fr: 'Philadelphie',
                },
                initials: 'PHI',
                emails: 'philadelphia@alveole.buzz',
                time: { 
                    zone: 'America/Toronto',
                    locale: 'en',
                    longFormat: 'dddd MMMM D YYYY H:mm',
                    shortFormat: 'H:mm',
                },
                location: {
                    lat: 39.983808,
                    lng: -75.104434,
                },
                greaterRegion: {
                    en: "United States",
                    fr: "États-Unis"
                },
            },
            9: {
                name: {
                    en: 'Washington, D.C.',
                    fr: 'Washington, D.C.',
                },
                initials: 'DC',
                emails: 'washington@alveole.buzz',
                time: { 
                    zone: 'America/Toronto',
                    locale: 'en',
                    longFormat: 'dddd MMMM D YYYY H:mm',
                    shortFormat: 'H:mm',
                },
                location: {
                    lat: 38.877383,
                    lng: -77.228662,
                },
                greaterRegion: {
                    en: "United States",
                    fr: "États-Unis"
                },
            },
            10: {
                name: {
                    en: 'Houston',
                    fr: 'Houston',
                },
                initials: 'HOU',
                emails: 'houston@alveole.buzz',
                time: { 
                    zone: 'America/Chicago',
                    locale: 'en',
                    longFormat: 'dddd MMMM D YYYY H:mm',
                    shortFormat: 'H:mm',
                }, 
                location: {
                    lat: 29.750870, 
                    lng: -95.342306,
                },
                greaterRegion: {
                    en: "United States",
                    fr: "États-Unis"
                },
            },
            11: {
                name: {
                    en: 'Dallas',
                    fr: 'Dallas',
                },
                initials: 'DAL',
                emails: 'dallas@alveole.buzz',
                time: { 
                    zone: 'America/Chicago',
                    locale: 'en',
                    longFormat: 'dddd MMMM D YYYY H:mm',
                    shortFormat: 'H:mm',
                },
                location: {
                    lat: 32.874810, 
                    lng: -96.764485,
                },
                greaterRegion: {
                    en: "United States",
                    fr: "États-Unis"
                },
            },
            12: {
                name: {
                    en: 'San Francisco',
                    fr: 'San Francisco',
                },
                initials: 'SF',
                emails: 'sanfrancisco@alveole.buzz',
                time: { 
                    zone: 'America/Vancouver',
                    locale: 'en',
                    longFormat: 'dddd MMMM D YYYY H:mm',
                    shortFormat: 'H:mm',
                },
                location: {
                    lat: 37.590383,
                    lng: -122.366027,
                },
                greaterRegion: {
                    en: "United States",
                    fr: "États-Unis"
                },
            },
            13: {
                name: {
                    en: 'Los Angeles',
                    fr: 'Los Angeles',
                },
                initials: 'LA',
                emails: 'losangeles@alveole.buzz',
                time: { 
                    zone: 'America/Vancouver',
                    locale: 'en',
                    longFormat: 'dddd MMMM D YYYY H:mm',
                    shortFormat: 'H:mm',
                },
                location: {
                    lat: 34.008812,
                    lng: -118.238258,
                },
                greaterRegion: {
                    en: "United States",
                    fr: "États-Unis"
                },
            },
            14: {
                name: {
                    en: 'Paris/Bretagne/Pays de la Loire',
                    fr: 'Paris/Bretagne/Pays de la Loire',
                },
                initials: 'PAR',
                emails: 'paris@alveole.buzz',
                time: { 
                    zone: 'Europe/Paris',
                    locale: 'fr',
                    longFormat: 'dddd MMMM D YYYY H:mm',
                    shortFormat: 'H:mm',
                },
                location: {
                    lat: 48.731463,
                    lng: 2.182743,
                },
                greaterRegion: {
                    en: "Europe",
                    fr: "Europe"
                },
            },
            15: {
                name: {
                    en: 'Austin',
                    fr: 'Austin',
                },
                initials: 'ATX',
                emails: 'austin@alveole.buzz',
                time: { 
                    zone: 'America/Chicago',
                    locale: 'en',
                    longFormat: 'dddd MMMM D YYYY H:mm',
                    shortFormat: 'H:mm',
                },
                location: {
                    lat: 30.263579,
                    lng: -97.749690,
                },
                greaterRegion: {
                    en: "United States",
                    fr: "États-Unis"
                },
            },
            16: {
                name: {
                    en: 'Denver',
                    fr: 'Denver',
                },
                initials: 'DEN',
                emails: 'denver@alveole.buzz',
                time: { 
                    zone: 'America/Edmonton',
                    locale: 'en',
                    longFormat: 'dddd MMMM D YYYY H:mm',
                    shortFormat: 'H:mm',
                },
                location: {
                    lat: 39.764,
                    lng: -105.135,
                },
                greaterRegion: {
                    en: "United States",
                    fr: "États-Unis"
                },
            },
            17: {
                name: {
                    en: 'Edmonton',
                    fr: 'Edmonton',
                },
                initials: 'EDM',
                emails: 'edmonton@alveole.buzz',
                time: { 
                    zone: 'America/Edmonton',
                    locale: 'en',
                    longFormat: 'dddd MMMM D YYYY H:mm',
                    shortFormat: 'H:mm',
                },
                location: {
                    lat: 53.555,
                    lng: -113.773,
                },
                greaterRegion: {
                    en: "Canada",
                    fr: "Canada"
                },
            },
            18: {
                name: {
                    en: 'Amsterdam',
                    fr: 'Amsterdam',
                },
                initials: 'AMS',
                emails: 'amsterdam@alveole.buzz',
                time: { 
                    zone: 'Europe/Amsterdam',
                    locale: 'nl',
                    longFormat: 'dddd MMMM D YYYY H:mm',
                    shortFormat: 'H:mm',
                },
                location: { // TODO
                    lat: null, 
                    lng: null,
                },
                greaterRegion: {
                    en: "Europe",
                    fr: "Europe"
                },
            },
            19: {
                name: {
                    en: 'Seattle',
                    fr: 'Seattle',
                },
                initials: 'SEA',
                emails: 'seattle@alveole.buzz',
                time: { 
                    zone: 'America/Vancouver',
                    locale: 'en',
                    longFormat: 'dddd MMMM D YYYY H:mm',
                    shortFormat: 'H:mm',
                },
                location: {
                    lat: 47.613,
                    lng: -122.482,
                },
                greaterRegion: {
                    en: "United States",
                    fr: "États-Unis"
                },
            },
            20: {
                name: {
                    en: 'Portland',
                    fr: 'Portland',
                },
                initials: 'PDX',
                emails: 'portland@alveole.buzz',
                time: { 
                    zone: 'America/Vancouver',
                    locale: 'en',
                    longFormat: 'dddd MMMM D YYYY H:mm',
                    shortFormat: 'H:mm',
                },
                location: {
                    lat: 45.543,
                    lng: -122.794,
                },
                greaterRegion: {
                    en: "United States",
                    fr: "États-Unis"
                },
            },
            21: {
                name: {
                    en: 'San Diego',
                    fr: 'San Diego',
                },
                initials: 'SD',
                emails: 'sandiego@alveole.buzz',
                time: { 
                    zone: 'America/Vancouver',
                    locale: 'en',
                    longFormat: 'dddd MMMM D YYYY H:mm',
                    shortFormat: 'H:mm',
                },
                location: {
                    lat: 32.825,
                    lng: -117.389, 
                },
                greaterRegion: {
                    en: "United States",
                    fr: "États-Unis"
                },
            },
            22: {
                name: {
                    en: 'Atlanta',
                    fr: 'Atlanta',
                },
                initials: 'ATL',
                emails: 'atlanta@alveole.buzz',
                time: { 
                    zone: 'America/Toronto',
                    locale: 'us',
                    longFormat: 'dddd MMMM D YYYY H:mm',
                    shortFormat: 'H:mm',
                },
                location: { // TODO
                    lat: null, 
                    lng: null,
                },
                greaterRegion: {
                    en: "United States",
                    fr: "États-Unis"
                },
            },
            23: {
                name: {
                    en: 'Baltimore',
                    fr: 'Baltimore',
                },
                initials: 'ATL',
                emails: 'baltimore@alveole.buzz',
                time: { 
                    zone: 'America/Toronto',
                    locale: 'us',
                    longFormat: 'dddd MMMM D YYYY H:mm',
                    shortFormat: 'H:mm',
                },
                location: { // TODO
                    lat: null, 
                    lng: null,
                },
                greaterRegion: {
                    en: "United States",
                    fr: "États-Unis"
                },
            },
            24: {
                name: {
                    en: 'Berlin',
                    fr: 'Berlin',
                },
                initials: 'BER',
                emails: 'berlin@alveole.buzz',
                time: { 
                    zone: 'Europe/Berlin',
                    locale: 'de',
                    longFormat: 'dddd MMMM D YYYY H:mm',
                    shortFormat: 'H:mm',
                },
                location: { // TODO
                    lat: null, 
                    lng: null,
                },
                greaterRegion: {
                    en: "Europe",
                    fr: "Europe"
                },
            },
            25: {
                name: {
                    en: 'Boston',
                    fr: 'Boston',
                },
                initials: 'BOS',
                emails: 'boston@alveole.buzz',
                time: { 
                    zone: 'America/Toronto',
                    locale: 'us',
                    longFormat: 'dddd MMMM D YYYY H:mm',
                    shortFormat: 'H:mm',
                },
                location: { // TODO
                    lat: null, 
                    lng: null,
                },
                greaterRegion: {
                    en: "United States",
                    fr: "États-Unis"
                },
            },
            26: {
                name: {
                    en: 'Brussels',
                    fr: 'Bruxelles',
                },
                initials: 'BRU',
                emails: 'brussels@alveole.buzz',
                time: { 
                    zone: 'Europe/Brussels',
                    locale: 'fr',
                    longFormat: 'dddd MMMM D YYYY H:mm',
                    shortFormat: 'H:mm',
                },
                location: { // TODO
                    lat: null, 
                    lng: null,
                },
                greaterRegion: {
                    en: "Europe",
                    fr: "Europe"
                },
            },
            27: {
                name: {
                    en: 'Charlotte',
                    fr: 'Charlotte',
                },
                initials: 'CLT',
                emails: 'charlotte@alveole.buzz',
                time: { 
                    zone: 'America/Toronto',
                    locale: 'us',
                    longFormat: 'dddd MMMM D YYYY H:mm',
                    shortFormat: 'H:mm',
                },
                location: { // TODO
                    lat: null, 
                    lng: null,
                },
                greaterRegion: {
                    en: "United States",
                    fr: "États-Unis"
                },
            },
            28: {
                name: {
                    en: 'Frankfurt',
                    fr: 'Francfort',
                },
                initials: 'FRA',
                emails: 'frankfurt@alveole.buzz',
                time: { 
                    zone: 'Europe/Berlin',
                    locale: 'de',
                    longFormat: 'dddd MMMM D YYYY H:mm',
                    shortFormat: 'H:mm',
                },
                location: { // TODO
                    lat: null, 
                    lng: null,
                },
                greaterRegion: {
                    en: "Europe",
                    fr: "Europe"
                },
            },
            29: {
                name: {
                    en: 'Halifax',
                    fr: 'Halifax',
                },
                initials: 'HFX',
                emails: 'halifax@alveole.buzz',
                time: { 
                    zone: 'America/Halifax',
                    locale: 'en',
                    longFormat: 'dddd MMMM D YYYY H:mm',
                    shortFormat: 'H:mm',
                },
                location: { // TODO
                    lat: null, 
                    lng: null,
                },
                greaterRegion: {
                    en: "Canada",
                    fr: "Canada"
                },
            },
            30: {
                name: {
                    en: 'Indianapolis',
                    fr: 'Indianapolis',
                },
                initials: 'IND',
                emails: 'indianapolis@alveole.buzz',
                time: { 
                    zone: 'America/Toronto',
                    locale: 'us',
                    longFormat: 'dddd MMMM D YYYY H:mm',
                    shortFormat: 'H:mm',
                },
                location: { // TODO
                    lat: null, 
                    lng: null,
                },
                greaterRegion: {
                    en: "United States",
                    fr: "États-Unis"
                },
            },
            31: {
                name: {
                    en: 'Kitchener/Waterloo',
                    fr: 'Kitchener/Waterloo',
                },
                initials: 'KW',
                emails: 'kitchenerwaterloo@alveole.buzz',
                time: { 
                    zone: 'America/Toronto',
                    locale: 'en',
                    longFormat: 'dddd MMMM D YYYY H:mm',
                    shortFormat: 'H:mm',
                },
                location: { // TODO
                    lat: null, 
                    lng: null,
                },
                greaterRegion: {
                    en: "Canada",
                    fr: "Canada"
                },
            },
            32: {
                name: {
                    en: 'London',
                    fr: 'Londre',
                },
                initials: 'LDN',
                emails: 'london@alveole.buzz',
                time: { 
                    zone: 'Europe/London',
                    locale: 'en-gb',
                    longFormat: 'dddd MMMM D YYYY H:mm',
                    shortFormat: 'H:mm',
                },
                location: { // TODO
                    lat: null, 
                    lng: null,
                },
                greaterRegion: {
                    en: "Europe",
                    fr: "Europe"
                },
            },
            33: {
                name: {
                    en: 'Minneapolis',
                    fr: 'Minneapolis',
                },
                initials: 'MSP',
                emails: 'minneapolis@alveole.buzz',
                time: { 
                    zone: 'America/Chicago',
                    locale: 'us',
                    longFormat: 'dddd MMMM D YYYY H:mm',
                    shortFormat: 'H:mm',
                },
                location: { // TODO
                    lat: null, 
                    lng: null,
                },
                greaterRegion: {
                    en: "United States",
                    fr: "États-Unis"
                },
            },
            34: {
                name: {
                    en: 'Nantes',
                    fr: 'Nantes',
                },
                initials: 'NTE',
                emails: 'nantes@alveole.buzz',
                time: { 
                    zone: 'Europe/Paris',
                    locale: 'fr',
                    longFormat: 'dddd MMMM D YYYY H:mm',
                    shortFormat: 'H:mm',
                },
                location: { // TODO
                    lat: null, 
                    lng: null,
                },
                greaterRegion: {
                    en: "Europe",
                    fr: "Europe"
                },
            },
            35: {
                name: {
                    en: 'Nashville',
                    fr: 'Nashville',
                },
                initials: 'NSH',
                emails: 'nashville@alveole.buzz',
                time: { 
                    zone: 'America/Chicago',
                    locale: 'us',
                    longFormat: 'dddd MMMM D YYYY H:mm',
                    shortFormat: 'H:mm',
                },
                location: { // TODO
                    lat: null, 
                    lng: null,
                },
                greaterRegion: {
                    en: "United States",
                    fr: "États-Unis"
                },
            },
            36: {
                name: {
                    en: 'Phoenix',
                    fr: 'Phoenix',
                },
                initials: 'PHO',
                emails: 'phoenix@alveole.buzz',
                time: { 
                    zone: 'America/Phoenix',
                    locale: 'us',
                    longFormat: 'dddd MMMM D YYYY H:mm',
                    shortFormat: 'H:mm',
                },
                location: { // TODO
                    lat: null, 
                    lng: null,
                },
                greaterRegion: {
                    en: "United States",
                    fr: "États-Unis"
                },
            },
            37: {
                name: {
                    en: 'Pittsburgh',
                    fr: 'Pittsburgh',
                },
                initials: 'PIT',
                emails: 'pittsburgh@alveole.buzz',
                time: { 
                    zone: 'America/Toronto',
                    locale: 'us',
                    longFormat: 'dddd MMMM D YYYY H:mm',
                    shortFormat: 'H:mm',
                },
                location: { // TODO
                    lat: null, 
                    lng: null,
                },
                greaterRegion: {
                    en: "United States",
                    fr: "États-Unis"
                },
            },
            38: {
                name: {
                    en: 'Sacramento',
                    fr: 'Sacramento',
                },
                initials: 'SAC',
                emails: 'sacramento@alveole.buzz',
                time: { 
                    zone: 'America/Vancouver',
                    locale: 'us',
                    longFormat: 'dddd MMMM D YYYY H:mm',
                    shortFormat: 'H:mm',
                },
                location: { // TODO
                    lat: null, 
                    lng: null,
                },
                greaterRegion: {
                    en: "United States",
                    fr: "États-Unis"
                },
            },
            39: {
                name: {
                    en: 'Victoria',
                    fr: 'Victoria',
                },
                initials: 'VIC',
                emails: 'victoria@alveole.buzz',
                time: { 
                    zone: 'America/Vancouver',
                    locale: 'en',
                    longFormat: 'dddd MMMM D YYYY H:mm',
                    shortFormat: 'H:mm',
                },
                location: { // TODO
                    lat: null, 
                    lng: null,
                },
                greaterRegion: {
                    en: "Canada",
                    fr: "Canada"
                },
            },
            40: {
                name: {
                    en: 'Louisville',
                    fr: 'Louisville',
                },
                initials: 'LOU',
                emails: 'kiera@alveole.buzz',
                time: { 
                    zone: 'America/Toronto',
                    locale: 'en',
                    longFormat: 'dddd MMMM D YYYY H:mm',
                    shortFormat: 'H:mm',
                },
                location: { // TODO
                    lat: null, 
                    lng: null,
                },
                greaterRegion: {
                    en: "United States",
                    fr: "États-Unis"
                },
            },
            41: {
                name: {
                    en: 'Albuquerque',
                    fr: 'Albuquerque',
                },
                initials: 'ABQ',
                emails: 'kiera@alveole.buzz',
                time: { 
                    zone: 'America/Edmonton',
                    locale: 'en',
                    longFormat: 'dddd MMMM D YYYY H:mm',
                    shortFormat: 'H:mm',
                },
                location: { // TODO
                    lat: null, 
                    lng: null,
                },
                greaterRegion: {
                    en: "United States",
                    fr: "États-Unis"
                },
            },
            42: {
                name: {
                    en: 'Cincinnati',
                    fr: 'Cincinnati',
                },
                initials: 'CIN',
                emails: 'kiera@alveole.buzz',
                time: { 
                    zone: 'America/Toronto',
                    locale: 'en',
                    longFormat: 'dddd MMMM D YYYY H:mm',
                    shortFormat: 'H:mm',
                },
                location: { // TODO
                    lat: null, 
                    lng: null,
                },
                greaterRegion: {
                    en: "United States",
                    fr: "États-Unis"
                },
            },
            43: {
                name: {
                    en: 'Helena',
                    fr: 'Helena',
                },
                initials: 'HLN',
                emails: 'kiera@alveole.buzz',
                time: { 
                    zone: 'America/Edmonton',
                    locale: 'en',
                    longFormat: 'dddd MMMM D YYYY H:mm',
                    shortFormat: 'H:mm',
                },
                location: { // TODO
                    lat: null, 
                    lng: null,
                },
                greaterRegion: {
                    en: "United States",
                    fr: "États-Unis"
                },
            },
            44: {
                name: {
                    en: 'Tulsa',
                    fr: 'Tulsa',
                },
                initials: 'TUL',
                emails: 'kiera@alveole.buzz',
                time: { 
                    zone: 'America/Chicago',
                    locale: 'en',
                    longFormat: 'dddd MMMM D YYYY H:mm',
                    shortFormat: 'H:mm',
                },
                location: { // TODO
                    lat: null, 
                    lng: null,
                },
                greaterRegion: {
                    en: "United States",
                    fr: "États-Unis"
                },
            },
            45: {
                name: {
                    en: 'Winnipeg',
                    fr: 'Winnipeg',
                },
                initials: 'YWG',
                emails: 'kiera@alveole.buzz',
                time: { 
                    zone: 'America/Chicago',
                    locale: 'en',
                    longFormat: 'dddd MMMM D YYYY H:mm',
                    shortFormat: 'H:mm',
                },
                location: { // TODO
                    lat: null, 
                    lng: null,
                },
                greaterRegion: {
                    en: "Canada",
                    fr: "Canada"
                },
            },
            46: {
                name: {
                    en: 'Richmond',
                    fr: 'Richmond',
                },
                initials: 'RMD',
                emails: 'kiera@alveole.buzz',
                time: { 
                    zone: 'America/Toronto',
                    locale: 'en',
                    longFormat: 'dddd MMMM D YYYY H:mm',
                    shortFormat: 'H:mm',
                },
                location: {
                    lat: null,
                    lng: null,
                },
                greaterRegion: {
                    en: "United States",
                    fr: "États-Unis"
                },
            },
            47: {
                name: {
                    en: 'Miami',
                    fr: 'Miami',
                },
                initials: 'MIA',
                emails: 'kiera@alveole.buzz',
                time: { 
                    zone: 'America/Toronto',
                    locale: 'en',
                    longFormat: 'dddd MMMM D YYYY H:mm',
                    shortFormat: 'H:mm',
                },
                location: {
                    lat: null,
                    lng: null,
                },
                greaterRegion: {
                    en: "United States",
                    fr: "États-Unis"
                },
            },
            unknown: {
                name: {
                    en: 'Unknown region',
                    fr: 'Region inconnu',
                },
                intials: 'UNK',
                emails: 'info@alveole.buzz',
                time: { 
                    zone: 'America/Toronto',
                    locale: 'en',
                    longFormat: 'dddd MMMM D YYYY H:mm',
                    shortFormat: 'H:mm',
                },
                location: {
                    lat: 45.533390,
                    lng: -73.618753,
                },
            },
        },
        rdvStatuses: {
            0: {
                name: {
                    en: 'Unconfirmed',
                    fr: 'Non confirmé',
                },
                color: 'red',
            },
            1: {
                name: {
                    en: 'Waiting',
                    fr: 'En attente',
                },
                color: 'blue',
            },
            2: {
                name: {
                    en: 'Client contacted',
                    fr: 'Client contacté',
                },
                color: 'orange',
            },
            3: {
                name: {
                    en: 'Confirmed',
                    fr: 'Confirmé',
                },
                color: 'green',
            },
            4: {
                name: {
                    en: 'Cancelled',
                    fr: 'Annulée',
                },
                color: 'black',
            },
            5: {
                name: {
                    en: 'Deleted',
                    fr: 'Supprimée',
                },
                color: 'black',
            },
            10: {
                name: {
                    en: 'Placed automatically',
                    fr: 'Placé automatiquement',
                },
                color: 'purple',
            },
            unknown: {
                name: {
                    en: 'Unknown rdv status',
                    fr: 'Statut rdv inconnu',
                },
                color: 'grey',
            },
        },
        visitTypes: {
            reg_ouverture_business: {
                id: "reg_ouverture_business",
                name: {
                    en: "Spring opening",
                    fr: "Ouverture du printemps",
                },
            },
            reg_division_business: {
                id: "reg_division_business",
                name: {
                    en: 'Splitting the colony',
                    fr: 'Division de la colonie',
                },
            },
            reg_inspection1: {
                id: "reg_inspection1",
                name: {
                    en: 'Inspection',
                    fr: 'Inspection',
                },
            },
            reg_inspection2: {
                id: "reg_inspection2",
                name: {
                    en: 'Inspection',
                    fr: 'Inspection',
                },
            },
            reg_ouverture: {
                id: 'reg_ouverture',
                name: {
                    en: 'Spring opening',
                    fr: 'Ouverture du printemps',
                },
                onlyReturning: true,
            },
            reg_check: {
                id: 'reg_check',
                name: {
                    en: 'Health check',
                    fr: 'Health check',
                },
                onlyReturning: true,
                recommendedDateRanges: {
                    0: [''], // montreal
                    1: [''], // quebec
                    2: [''], // toronto
                    3: [''], // ottawa
                    4: [''], // calgary
                    5: [''], // vancouver
                    6: [''], // chicago
                    7: [''], // nyc
                    8: [''], // philly
                    9: [''], // dc
                    10: [''], // houston
                    11: [''], // dallas
                    12: [''], // sf
                    13: [''], // la
                    14: [''], // paris
                    15: [''], // austin
                    16: [''], // denver
                    17: [''], // edmonton
                    18: [''], // amsterdam
                    19: [''], // seattle
                    20: [''], // portland
                    21: [''], // san diego
                    22: [''], // atlanta
                    23: [''], // baltimore 
                    24: [''], // berlin
                    25: [''], // boston 
                    26: [''], // brussels
                    27: [''], // charlotte
                    28: [''], // frankfurt
                    29: [''], // halifax
                    30: [''], // indianapolis
                    31: [''], // kitchenerwaterloo
                    32: [''], // london
                    33: [''], // minneapolis
                    34: [''], // nantes
                    35: [''], // nashville
                    36: [''], // phoenix
                    37: [''], // pittsburgh
                    38: [''], // sacramento
                    39: [''], // victoria
                    40: [''], // louisville
                    41: [''], // albuquerque
                    42: [''], // cincinnati
                    43: [''], // helena
                    44: [''], // tulsa
                    45: [''], // winnipeg
                    46: [''], // richmond
                    47: [''], // miami
                },
            },
            reg_check2: {
                id: 'reg_check2',
                name: {
                    en: 'Health check',
                    fr: 'Health check',
                },
                onlyReturning: true,
                recommendedDateRanges: {
                    0: [''], // montreal
                    1: [''], // quebec
                    2: [''], // toronto
                    3: [''], // ottawa
                    4: [''], // calgary
                    5: [''], // vancouver
                    6: [''], // chicago
                    7: [''], // nyc
                    8: [''], // philly
                    9: [''], // dc
                    10: [''], // houston
                    11: [''], // dallas
                    12: [''], // sf
                    13: [''], // la
                    14: [''], // paris
                    15: [''], // austin
                    16: [''], // denver
                    17: [''], // edmonton
                    18: [''], // amsterdam
                    19: [''], // seattle
                    20: [''], // portland
                    21: [''], // san diego
                    22: [''], // atlanta
                    23: [''], // baltimore 
                    24: [''], // berlin
                    25: [''], // boston 
                    26: [''], // brussels
                    27: [''], // charlotte
                    28: [''], // frankfurt
                    29: [''], // halifax
                    30: [''], // indianapolis
                    31: [''], // kitchenerwaterloo
                    32: [''], // london
                    33: [''], // minneapolis
                    34: [''], // nantes
                    35: [''], // nashville
                    36: [''], // phoenix
                    37: [''], // pittsburgh
                    38: [''], // sacramento
                    39: [''], // victoria
                    40: [''], // louisville
                    41: [''], // albuquerque
                    42: [''], // cincinnati
                    43: [''], // helena
                    44: [''], // tulsa
                    45: [''], // winnipeg
                    46: [''], // richmond
                    47: [''], // miami
                },
            },
            reg_inspection: {
                id: 'reg_inspection',
                name: {
                    en: 'Growth check',
                    fr: 'Growth check',
                },
                onlyReturning: true,
                recommendedDateRanges: {
                    0: [''], // montreal
                    1: [''], // quebec
                    2: [''], // toronto
                    3: [''], // ottawa
                    4: [''], // calgary
                    5: [''], // vancouver
                    6: [''], // chicago
                    7: [''], // nyc
                    8: [''], // philly
                    9: [''], // dc
                    10: [''], // houston
                    11: [''], // dallas
                    12: [''], // sf
                    13: [''], // la
                    14: [''], // paris
                    15: [''], // austin
                    16: [''], // denver
                    17: [''], // edmonton
                    18: [''], // amsterdam
                    19: [''], // seattle
                    20: [''], // portland
                    21: [''], // san diego
                    22: [''], // atlanta
                    23: [''], // baltimore 
                    24: [''], // berlin
                    25: [''], // boston 
                    26: [''], // brussels
                    27: [''], // charlotte
                    28: [''], // frankfurt
                    29: [''], // halifax
                    30: [''], // indianapolis
                    31: [''], // kitchenerwaterloo
                    32: [''], // london
                    33: [''], // minneapolis
                    34: [''], // nantes
                    35: [''], // nashville
                    36: [''], // phoenix
                    37: [''], // pittsburgh
                    38: [''], // sacramento
                    39: [''], // victoria
                    40: [''], // louisville
                    41: [''], // albuquerque
                    42: [''], // cincinnati
                    43: [''], // helena
                    44: [''], // tulsa
                    45: [''], // winnipeg
                    46: [''], // richmond
                    47: [''], // miami
                },
            },
            reg_equalizing1: {
                id: 'reg_equalizing1',
                name: {
                    en: 'Equalizing',
                    fr: 'Equalizing',
                },
                onlyReturning: true,
                recommendedDateRanges: {
                    0: [''], // montreal
                    1: [''], // quebec
                    2: [''], // toronto
                    3: [''], // ottawa
                    4: [''], // calgary
                    5: [''], // vancouver
                    6: [''], // chicago
                    7: [''], // nyc
                    8: [''], // philly
                    9: [''], // dc
                    10: [''], // houston
                    11: [''], // dallas
                    12: [''], // sf
                    13: [''], // la
                    14: [''], // paris
                    15: [''], // austin
                    16: [''], // denver
                    17: [''], // edmonton
                    18: [''], // amsterdam
                    19: [''], // seattle
                    20: [''], // portland
                    21: [''], // san diego
                    22: [''], // atlanta
                    23: [''], // baltimore 
                    24: [''], // berlin
                    25: [''], // boston 
                    26: [''], // brussels
                    27: [''], // charlotte
                    28: [''], // frankfurt
                    29: [''], // halifax
                    30: [''], // indianapolis
                    31: [''], // kitchenerwaterloo
                    32: [''], // london
                    33: [''], // minneapolis
                    34: [''], // nantes
                    35: [''], // nashville
                    36: [''], // phoenix
                    37: [''], // pittsburgh
                    38: [''], // sacramento
                    39: [''], // victoria
                    40: [''], // louisville
                    41: [''], // albuquerque
                    42: [''], // cincinnati
                    43: [''], // helena
                    44: [''], // tulsa
                    45: [''], // winnipeg
                    46: [''], // richmond
                    43: [''], // helena
                    44: [''], // tulsa
                    45: [''], // winnipeg
                    46: [''], // richmond
                    47: [''], // miami
                },
            },
            reg_hausses: {
                id: 'reg_hausses',
                name: {
                    en: 'Poser les hausses',
                    fr: 'Poser les hausses',
                },
                onlyReturning: true,
            },
            reg_division: {
                id: 'reg_division',
                name: {
                    en: 'Splitting the colony',
                    fr: 'Division de la colonie',
                },
                onlyReturning: true,
                recommendedDateRanges: {
                    0: [''], // montreal
                    1: [''], // quebec
                    2: [''], // toronto
                    3: [''], // ottawa
                    4: [''], // calgary
                    5: [''], // vancouver
                    6: [''], // chicago
                    7: [''], // nyc
                    8: [''], // philly
                    9: [''], // dc
                    10: [''], // houston
                    11: [''], // dallas
                    12: [''], // sf
                    13: [''], // la
                    14: [''], // paris
                    15: [''], // austin
                    16: [''], // denver
                    17: [''], // edmonton
                    18: [''], // amsterdam
                    19: [''], // seattle
                    20: [''], // portland
                    21: [''], // san diego
                    22: [''], // atlanta
                    23: [''], // baltimore 
                    24: [''], // berlin
                    25: [''], // boston 
                    26: [''], // brussels
                    27: [''], // charlotte
                    28: [''], // frankfurt
                    29: [''], // halifax
                    30: [''], // indianapolis
                    31: [''], // kitchenerwaterloo
                    32: [''], // london
                    33: [''], // minneapolis
                    34: [''], // nantes
                    35: [''], // nashville
                    36: [''], // phoenix
                    37: [''], // pittsburgh
                    38: [''], // sacramento
                    39: [''], // victoria
                    40: [''], // louisville
                    41: [''], // albuquerque
                    42: [''], // cincinnati
                    43: [''], // helena
                    44: [''], // tulsa
                    45: [''], // winnipeg
                    46: [''], // richmond
                    47: [''], // miami
                },
            },
            reg_installation: {
                id: 'reg_installation',
                name: {
                    en: 'Installation of the hive',
                    fr: 'Installation de la ruche',
                },
                onlyNew: true,
                recommendedDateRanges: {
                    0: [''], // montreal
                    1: [''], // quebec
                    2: [''], // toronto
                    3: [''], // ottawa
                    4: [''], // calgary
                    5: [''], // vancouver
                    6: [''], // chicago
                    7: [''], // nyc
                    8: [''], // philly
                    9: [''], // dc
                    10: [''], // houston
                    11: [''], // dallas
                    12: [''], // sf
                    13: [''], // la
                    14: [''], // paris
                    15: [''], // austin
                    16: [''], // denver
                    17: [''], // edmonton
                    18: [''], // amsterdam
                    19: [''], // seattle
                    20: [''], // portland
                    21: [''], // san diego
                    22: [''], // atlanta
                    23: [''], // baltimore 
                    24: [''], // berlin
                    25: [''], // boston 
                    26: [''], // brussels
                    27: [''], // charlotte
                    28: [''], // frankfurt
                    29: [''], // halifax
                    30: [''], // indianapolis
                    31: [''], // kitchenerwaterloo
                    32: [''], // london
                    33: [''], // minneapolis
                    34: [''], // nantes
                    35: [''], // nashville
                    36: [''], // phoenix
                    37: [''], // pittsburgh
                    38: [''], // sacramento
                    39: [''], // victoria
                    40: [''], // louisville
                    41: [''], // albuquerque
                    42: [''], // cincinnati
                    43: [''], // helena
                    44: [''], // tulsa
                    45: [''], // winnipeg
                    46: [''], // richmond
                    47: [''], // miami
                },
            },
            reg_summer: {
                id: 'reg_summer',
                name: {
                    en: 'Summer visit',
                    fr: `Visite d'été`,
                },
                onlyReturning: true,
                recommendedDateRanges: {
                    0: [''], // montreal
                    1: [''], // quebec
                    2: [''], // toronto
                    3: [''], // ottawa
                    4: [''], // calgary
                    5: [''], // vancouver
                    6: [''], // chicago
                    7: [''], // nyc
                    8: [''], // philly
                    9: [''], // dc
                    10: [''], // houston
                    11: [''], // dallas
                    12: [''], // sf
                    13: [''], // la
                    14: [''], // paris
                    15: [''], // austin
                    16: [''], // denver
                    17: [''], // edmonton
                    18: [''], // amsterdam
                    19: [''], // seattle
                    20: [''], // portland
                    21: [''], // san diego
                    22: [''], // atlanta
                    23: [''], // baltimore 
                    24: [''], // berlin
                    25: [''], // boston 
                    26: [''], // brussels
                    27: [''], // charlotte
                    28: [''], // frankfurt
                    29: [''], // halifax
                    30: [''], // indianapolis
                    31: [''], // kitchenerwaterloo
                    32: [''], // london
                    33: [''], // minneapolis
                    34: [''], // nantes
                    35: [''], // nashville
                    36: [''], // phoenix
                    37: [''], // pittsburgh
                    38: [''], // sacramento
                    39: [''], // victoria
                    40: [''], // louisville
                    41: [''], // albuquerque
                    42: [''], // cincinnati
                    43: [''], // helena
                    44: [''], // tulsa
                    45: [''], // winnipeg
                    46: [''], // richmond
                    47: [''], // miami
                },
            },
            reg_prevention: {
                id: 'reg_prevention',
                name: {
                    en: 'Swarm prevention',
                    fr: 'Prévention de l’essaimage',
                },
                recommendedDateRanges: {
                    0: [''], // montreal
                    1: [''], // quebec
                    2: [''], // toronto
                    3: [''], // ottawa
                    4: [''], // calgary
                    5: [''], // vancouver
                    6: [''], // chicago
                    7: [''], // nyc
                    8: [''], // philly
                    9: [''], // dc
                    10: [''], // houston
                    11: [''], // dallas
                    12: [''], // sf
                    13: [''], // la
                    14: [''], // paris
                    15: [''], // austin
                    16: [''], // denver
                    17: [''], // edmonton
                    18: [''], // amsterdam
                    19: [''], // seattle
                    20: [''], // portland
                    21: [''], // san diego
                    22: [''], // atlanta
                    23: [''], // baltimore 
                    24: [''], // berlin
                    25: [''], // boston 
                    26: [''], // brussels
                    27: [''], // charlotte
                    28: [''], // frankfurt
                    29: [''], // halifax
                    30: [''], // indianapolis
                    31: [''], // kitchenerwaterloo
                    32: [''], // london
                    33: [''], // minneapolis
                    34: [''], // nantes
                    35: [''], // nashville
                    36: [''], // phoenix
                    37: [''], // pittsburgh
                    38: [''], // sacramento
                    39: [''], // victoria
                    40: [''], // louisville
                    41: [''], // albuquerque
                    42: [''], // cincinnati
                    43: [''], // helena
                    44: [''], // tulsa
                    45: [''], // winnipeg
                    46: [''], // richmond
                    47: [''], // miami
                },
            },
            reg_spotting: {
                id: 'reg_spotting',
                name: {
                    en: 'Queen spotting',
                    fr: 'Trouver la reine',
                },
                recommendedDateRanges: {
                    0: [''], // montreal
                    1: [''], // quebec
                    2: [''], // toronto
                    3: [''], // ottawa
                    4: [''], // calgary
                    5: [''], // vancouver
                    6: [''], // chicago
                    7: [''], // nyc
                    8: [''], // philly
                    9: [''], // dc
                    10: [''], // houston
                    11: [''], // dallas
                    12: [''], // sf
                    13: [''], // la
                    14: [''], // paris
                    15: [''], // austin
                    16: [''], // denver
                    17: [''], // edmonton
                    18: [''], // amsterdam
                    19: [''], // seattle
                    20: [''], // portland
                    21: [''], // san diego
                    22: [''], // atlanta
                    23: [''], // baltimore 
                    24: [''], // berlin
                    25: [''], // boston 
                    26: [''], // brussels
                    27: [''], // charlotte
                    28: [''], // frankfurt
                    29: [''], // halifax
                    30: [''], // indianapolis
                    31: [''], // kitchenerwaterloo
                    32: [''], // london
                    33: [''], // minneapolis
                    34: [''], // nantes
                    35: [''], // nashville
                    36: [''], // phoenix
                    37: [''], // pittsburgh
                    38: [''], // sacramento
                    39: [''], // victoria
                    40: [''], // louisville
                    41: [''], // albuquerque
                    42: [''], // cincinnati
                    43: [''], // helena
                    44: [''], // tulsa
                    45: [''], // winnipeg
                    46: [''], // richmond
                    47: [''], // miami
                },
            },
            reg_nectar: {
                id: 'reg_nectar',
                name: {
                    en: 'Nectar flow',
                    fr: 'Nectar flow',
                },
            },
            reg_preparation: {
                id: 'reg_preparation',
                name: {
                    en: 'Preparing for the harvest',
                    fr: 'Préparation à la récolte',
                },
                recommendedDateRanges: {
                    0: [''], // montreal
                    1: [''], // quebec
                    2: [''], // toronto
                    3: [''], // ottawa
                    4: [''], // calgary
                    5: [''], // vancouver
                    6: [''], // chicago
                    7: [''], // nyc
                    8: [''], // philly
                    9: [''], // dc
                    10: [''], // houston
                    11: [''], // dallas
                    12: [''], // sf
                    13: [''], // la
                    14: [''], // paris
                    15: [''], // austin
                    16: [''], // denver
                    17: [''], // edmonton
                    18: [''], // amsterdam
                    19: [''], // seattle
                    20: [''], // portland
                    21: [''], // san diego
                    22: [''], // atlanta
                    23: [''], // baltimore 
                    24: [''], // berlin
                    25: [''], // boston 
                    26: [''], // brussels
                    27: [''], // charlotte
                    28: [''], // frankfurt
                    29: [''], // halifax
                    30: [''], // indianapolis
                    31: [''], // kitchenerwaterloo
                    32: [''], // london
                    33: [''], // minneapolis
                    34: [''], // nantes
                    35: [''], // nashville
                    36: [''], // phoenix
                    37: [''], // pittsburgh
                    38: [''], // sacramento
                    39: [''], // victoria
                    40: [''], // louisville
                    41: [''], // albuquerque
                    42: [''], // cincinnati
                    43: [''], // helena
                    44: [''], // tulsa
                    45: [''], // winnipeg
                    46: [''], // richmond
                    47: [''], // miami
                },
            },
            reg_recolte: {
                id: 'reg_recolte',
                name: {
                    en: 'Honey harvest and treatment',
                    fr: 'Récolte du miel',
                },
                recommendedDateRanges: {
                    0: [''], // montreal
                    1: [''], // quebec
                    2: [''], // toronto
                    3: [''], // ottawa
                    4: [''], // calgary
                    5: [''], // vancouver
                    6: [''], // chicago
                    7: [''], // nyc
                    8: [''], // philly
                    9: [''], // dc
                    10: [''], // houston
                    11: [''], // dallas
                    12: [''], // sf
                    13: [''], // la
                    14: [''], // paris
                    15: [''], // austin
                    16: [''], // denver
                    17: [''], // edmonton
                    18: [''], // amsterdam
                    19: [''], // seattle
                    20: [''], // portland
                    21: [''], // san diego
                    22: [''], // atlanta
                    23: [''], // baltimore 
                    24: [''], // berlin
                    25: [''], // boston 
                    26: [''], // brussels
                    27: [''], // charlotte
                    28: [''], // frankfurt
                    29: [''], // halifax
                    30: [''], // indianapolis
                    31: [''], // kitchenerwaterloo
                    32: [''], // london
                    33: [''], // minneapolis
                    34: [''], // nantes
                    35: [''], // nashville
                    36: [''], // phoenix
                    37: [''], // pittsburgh
                    38: [''], // sacramento
                    39: [''], // victoria
                    40: [''], // louisville
                    41: [''], // albuquerque
                    42: [''], // cincinnati
                    43: [''], // helena
                    44: [''], // tulsa
                    45: [''], // winnipeg
                    46: [''], // richmond
                    47: [''], // miami
                },
            },
            reg_recolte2: {
                id: 'reg_recolte2',
                name: {
                    en: 'Honey harvest and treatment',
                    fr: 'Récolte du miel',
                },
                recommendedDateRanges: {
                    0: [''], // montreal
                    1: [''], // quebec
                    2: [''], // toronto
                    3: [''], // ottawa
                    4: [''], // calgary
                    5: [''], // vancouver
                    6: [''], // chicago
                    7: [''], // nyc
                    8: [''], // philly
                    9: [''], // dc
                    10: [''], // houston
                    11: [''], // dallas
                    12: [''], // sf
                    13: [''], // la
                    14: [''], // paris
                    15: [''], // austin
                    16: [''], // denver
                    17: [''], // edmonton
                    18: [''], // amsterdam
                    19: [''], // seattle
                    20: [''], // portland
                    21: [''], // san diego
                    22: [''], // atlanta
                    23: [''], // baltimore 
                    24: [''], // berlin
                    25: [''], // boston 
                    26: [''], // brussels
                    27: [''], // charlotte
                    28: [''], // frankfurt
                    29: [''], // halifax
                    30: [''], // indianapolis
                    31: [''], // kitchenerwaterloo
                    32: [''], // london
                    33: [''], // minneapolis
                    34: [''], // nantes
                    35: [''], // nashville
                    36: [''], // phoenix
                    37: [''], // pittsburgh
                    38: [''], // sacramento
                    39: [''], // victoria
                    40: [''], // louisville
                    41: [''], // albuquerque
                    42: [''], // cincinnati
                    43: [''], // helena
                    44: [''], // tulsa
                    45: [''], // winnipeg
                    46: [''], // richmond
                    47: [''], // miami
                },
            },
            reg_dearth: {
                id: 'reg_dearth',
                name: {
                    en: 'Treatment & dearth',
                    fr: 'Treatment & dearth',
                },
            },
            reg_nourissage: {
                id: 'reg_nourissage',
                name: {
                    en: 'Feeding the colony',
                    fr: 'Nourrissage de la colonie',
                },
                recommendedDateRanges: {
                    0: [''], // montreal
                    1: [''], // quebec
                    2: [''], // toronto
                    3: [''], // ottawa
                    4: [''], // calgary
                    5: [''], // vancouver
                    6: [''], // chicago
                    7: [''], // nyc
                    8: [''], // philly
                    9: [''], // dc
                    10: [''], // houston
                    11: [''], // dallas
                    12: [''], // sf
                    13: [''], // la
                    14: [''], // paris
                    15: [''], // austin
                    16: [''], // denver
                    17: [''], // edmonton
                    18: [''], // amsterdam
                    19: [''], // seattle
                    20: [''], // portland
                    21: [''], // san diego
                    22: [''], // atlanta
                    23: [''], // baltimore 
                    24: [''], // berlin
                    25: [''], // boston 
                    26: [''], // brussels
                    27: [''], // charlotte
                    28: [''], // frankfurt
                    29: [''], // halifax
                    30: [''], // indianapolis
                    31: [''], // kitchenerwaterloo
                    32: [''], // london
                    33: [''], // minneapolis
                    34: [''], // nantes
                    35: [''], // nashville
                    36: [''], // phoenix
                    37: [''], // pittsburgh
                    38: [''], // sacramento
                    39: [''], // victoria
                    40: [''], // louisville
                    41: [''], // albuquerque
                    42: [''], // cincinnati
                    43: [''], // helena
                    44: [''], // tulsa
                    45: [''], // winnipeg
                    46: [''], // richmond
                    47: [''], // miami
                },
            },
            reg_nourissage2: {
                id: 'reg_nourissage2',
                name: {
                    en: 'Feeding the colony',
                    fr: 'Nourrissage de la colonie',
                },
                recommendedDateRanges: {
                    0: [''], // montreal
                    1: [''], // quebec
                    2: [''], // toronto
                    3: [''], // ottawa
                    4: [''], // calgary
                    5: [''], // vancouver
                    6: [''], // chicago
                    7: [''], // nyc
                    8: [''], // philly
                    9: [''], // dc
                    10: [''], // houston
                    11: [''], // dallas
                    12: [''], // sf
                    13: [''], // la
                    14: [''], // paris
                    15: [''], // austin
                    16: [''], // denver
                    17: [''], // edmonton
                    18: [''], // amsterdam
                    19: [''], // seattle
                    20: [''], // portland
                    21: [''], // san diego
                    22: [''], // atlanta
                    23: [''], // baltimore 
                    24: [''], // berlin
                    25: [''], // boston 
                    26: [''], // brussels
                    27: [''], // charlotte
                    28: [''], // frankfurt
                    29: [''], // halifax
                    30: [''], // indianapolis
                    31: [''], // kitchenerwaterloo
                    32: [''], // london
                    33: [''], // minneapolis
                    34: [''], // nantes
                    35: [''], // nashville
                    36: [''], // phoenix
                    37: [''], // pittsburgh
                    38: [''], // sacramento
                    39: [''], // victoria
                    40: [''], // louisville
                    41: [''], // albuquerque
                    42: [''], // cincinnati
                    43: [''], // helena
                    44: [''], // tulsa
                    45: [''], // winnipeg
                    46: [''], // richmond
                    47: [''], // miami
                },
            },
            reg_nourissage3: {
                id: 'reg_nourissage3',
                name: {
                    en: 'Feeding the colony',
                    fr: 'Nourrissage de la colonie',
                },
                recommendedDateRanges: {
                    0: [''], // montreal
                    1: [''], // quebec
                    2: [''], // toronto
                    3: [''], // ottawa
                    4: [''], // calgary
                    5: [''], // vancouver
                    6: [''], // chicago
                    7: [''], // nyc
                    8: [''], // philly
                    9: [''], // dc
                    10: [''], // houston
                    11: [''], // dallas
                    12: [''], // sf
                    13: [''], // la
                    14: [''], // paris
                    15: [''], // austin
                    16: [''], // denver
                    17: [''], // edmonton
                    18: [''], // amsterdam
                    19: [''], // seattle
                    20: [''], // portland
                    21: [''], // san diego
                    22: [''], // atlanta
                    23: [''], // baltimore 
                    24: [''], // berlin
                    25: [''], // boston 
                    26: [''], // brussels
                    27: [''], // charlotte
                    28: [''], // frankfurt
                    29: [''], // halifax
                    30: [''], // indianapolis
                    31: [''], // kitchenerwaterloo
                    32: [''], // london
                    33: [''], // minneapolis
                    34: [''], // nantes
                    35: [''], // nashville
                    36: [''], // phoenix
                    37: [''], // pittsburgh
                    38: [''], // sacramento
                    39: [''], // victoria
                    40: [''], // louisville
                    41: [''], // albuquerque
                    42: [''], // cincinnati
                    43: [''], // helena
                    44: [''], // tulsa
                    45: [''], // winnipeg
                    46: [''], // richmond
                    47: [''], // miami
                },
            },
            reg_division2: {
                id: 'reg_division2',
                name: {
                    en: 'Splitting the colony',
                    fr: 'Division de la colonie',
                },
                recommendedDateRanges: {
                    0: [''], // montreal
                    1: [''], // quebec
                    2: [''], // toronto
                    3: [''], // ottawa
                    4: [''], // calgary
                    5: [''], // vancouver
                    6: [''], // chicago
                    7: [''], // nyc
                    8: [''], // philly
                    9: [''], // dc
                    10: [''], // houston
                    11: [''], // dallas
                    12: [''], // sf
                    13: [''], // la
                    14: [''], // paris
                    15: [''], // austin
                    16: [''], // denver
                    17: [''], // edmonton
                    18: [''], // amsterdam
                    19: [''], // seattle
                    20: [''], // portland
                    21: [''], // san diego
                    22: [''], // atlanta
                    23: [''], // baltimore 
                    24: [''], // berlin
                    25: [''], // boston 
                    26: [''], // brussels
                    27: [''], // charlotte
                    28: [''], // frankfurt
                    29: [''], // halifax
                    30: [''], // indianapolis
                    31: [''], // kitchenerwaterloo
                    32: [''], // london
                    33: [''], // minneapolis
                    34: [''], // nantes
                    35: [''], // nashville
                    36: [''], // phoenix
                    37: [''], // pittsburgh
                    38: [''], // sacramento
                    39: [''], // victoria
                    40: [''], // louisville
                    41: [''], // albuquerque
                    42: [''], // cincinnati
                    43: [''], // helena
                    44: [''], // tulsa
                    45: [''], // winnipeg
                    46: [''], // richmond
                    47: [''], // miami
                },
            },
            reg_equalizing2: {
                id: 'reg_equalizing2',
                name: {
                    en: 'Equalizing',
                    fr: 'Equalizing',
                },
                recommendedDateRanges: {
                    0: [''], // montreal
                    1: [''], // quebec
                    2: [''], // toronto
                    3: [''], // ottawa
                    4: [''], // calgary
                    5: [''], // vancouver
                    6: [''], // chicago
                    7: [''], // nyc
                    8: [''], // philly
                    9: [''], // dc
                    10: [''], // houston
                    11: [''], // dallas
                    12: [''], // sf
                    13: [''], // la
                    14: [''], // paris
                    15: [''], // austin
                    16: [''], // denver
                    17: [''], // edmonton
                    18: [''], // amsterdam
                    19: [''], // seattle
                    20: [''], // portland
                    21: [''], // san diego
                    22: [''], // atlanta
                    23: [''], // baltimore 
                    24: [''], // berlin
                    25: [''], // boston 
                    26: [''], // brussels
                    27: [''], // charlotte
                    28: [''], // frankfurt
                    29: [''], // halifax
                    30: [''], // indianapolis
                    31: [''], // kitchenerwaterloo
                    32: [''], // london
                    33: [''], // minneapolis
                    34: [''], // nantes
                    35: [''], // nashville
                    36: [''], // phoenix
                    37: [''], // pittsburgh
                    38: [''], // sacramento
                    39: [''], // victoria
                    40: [''], // louisville
                    41: [''], // albuquerque
                    42: [''], // cincinnati
                    43: [''], // helena
                    44: [''], // tulsa
                    45: [''], // winnipeg
                    46: [''], // richmond
                    47: [''], // miami
                },
            },
            reg_inspection2: {
                id: 'reg_inspection2',
                name: {
                    en: 'Growth check',
                    fr: 'Growth check',
                },
                recommendedDateRanges: {
                    0: [''], // montreal
                    1: [''], // quebec
                    2: [''], // toronto
                    3: [''], // ottawa
                    4: [''], // calgary
                    5: [''], // vancouver
                    6: [''], // chicago
                    7: [''], // nyc
                    8: [''], // philly
                    9: [''], // dc
                    10: [''], // houston
                    11: [''], // dallas
                    12: [''], // sf
                    13: [''], // la
                    14: [''], // paris
                    15: [''], // austin
                    16: [''], // denver
                    17: [''], // edmonton
                    18: [''], // amsterdam
                    19: [''], // seattle
                    20: [''], // portland
                    21: [''], // san diego
                    22: [''], // atlanta
                    23: [''], // baltimore 
                    24: [''], // berlin
                    25: [''], // boston 
                    26: [''], // brussels
                    27: [''], // charlotte
                    28: [''], // frankfurt
                    29: [''], // halifax
                    30: [''], // indianapolis
                    31: [''], // kitchenerwaterloo
                    32: [''], // london
                    33: [''], // minneapolis
                    34: [''], // nantes
                    35: [''], // nashville
                    36: [''], // phoenix
                    37: [''], // pittsburgh
                    38: [''], // sacramento
                    39: [''], // victoria
                    40: [''], // louisville
                    41: [''], // albuquerque
                    42: [''], // cincinnati
                    43: [''], // helena
                    44: [''], // tulsa
                    45: [''], // winnipeg
                    46: [''], // richmond
                    47: [''], // miami
                },
            },
            reg_hivernage: {
                id: 'reg_hivernage',
                name: {
                    en: 'Wintering the hive',
                    fr: 'Hivernage de la ruche',
                },
                recommendedDateRanges: {
                    0: [''], // montreal
                    1: [''], // quebec
                    2: [''], // toronto
                    3: [''], // ottawa
                    4: [''], // calgary
                    5: [''], // vancouver
                    6: [''], // chicago
                    7: [''], // nyc
                    8: [''], // philly
                    9: [''], // dc
                    10: [''], // houston
                    11: [''], // dallas
                    12: [''], // sf
                    13: [''], // la
                    14: [''], // paris
                    15: [''], // austin
                    16: [''], // denver
                    17: [''], // edmonton
                    18: [''], // amsterdam
                    19: [''], // seattle
                    20: [''], // portland
                    21: [''], // san diego
                    22: [''], // atlanta
                    23: [''], // baltimore 
                    24: [''], // berlin
                    25: [''], // boston 
                    26: [''], // brussels
                    27: [''], // charlotte
                    28: [''], // frankfurt
                    29: [''], // halifax
                    30: [''], // indianapolis
                    31: [''], // kitchenerwaterloo
                    32: [''], // london
                    33: [''], // minneapolis
                    34: [''], // nantes
                    35: [''], // nashville
                    36: [''], // phoenix
                    37: [''], // pittsburgh
                    38: [''], // sacramento
                    39: [''], // victoria
                    40: [''], // louisville
                    41: [''], // albuquerque
                    42: [''], // cincinnati
                    43: [''], // helena
                    44: [''], // tulsa
                    45: [''], // winnipeg
                    46: [''], // richmond
                    47: [''], // miami
                },
            },
            reg_hivernage2: {
                id: 'reg_hivernage2',
                name: {
                    en: 'Wintering the hive',
                    fr: 'Hivernage de la ruche',
                },
                recommendedDateRanges: {
                    0: [''], // montreal
                    1: [''], // quebec
                    2: [''], // toronto
                    3: [''], // ottawa
                    4: [''], // calgary
                    5: [''], // vancouver
                    6: [''], // chicago
                    7: [''], // nyc
                    8: [''], // philly
                    9: [''], // dc
                    10: [''], // houston
                    11: [''], // dallas
                    12: [''], // sf
                    13: [''], // la
                    14: [''], // paris
                    15: [''], // austin
                    16: [''], // denver
                    17: [''], // edmonton
                    18: [''], // amsterdam
                    19: [''], // seattle
                    20: [''], // portland
                    21: [''], // san diego
                    22: [''], // atlanta
                    23: [''], // baltimore 
                    24: [''], // berlin
                    25: [''], // boston 
                    26: [''], // brussels
                    27: [''], // charlotte
                    28: [''], // frankfurt
                    29: [''], // halifax
                    30: [''], // indianapolis
                    31: [''], // kitchenerwaterloo
                    32: [''], // london
                    33: [''], // minneapolis
                    34: [''], // nantes
                    35: [''], // nashville
                    36: [''], // phoenix
                    37: [''], // pittsburgh
                    38: [''], // sacramento
                    39: [''], // victoria
                    40: [''], // louisville
                    41: [''], // albuquerque
                    42: [''], // cincinnati
                    43: [''], // helena
                    44: [''], // tulsa
                    45: [''], // winnipeg
                    46: [''], // richmond
                    47: [''], // miami
                },
            },
            reg_weight: {
                id: 'reg_weight',
                name: {
                    en: 'Weight check',
                    fr: 'Vérifier du poids',
                },
                recommendedDateRanges: {
                    0: [''], // montreal
                    1: [''], // quebec
                    2: [''], // toronto
                    3: [''], // ottawa
                    4: [''], // calgary
                    5: [''], // vancouver
                    6: [''], // chicago
                    7: [''], // nyc
                    8: [''], // philly
                    9: [''], // dc
                    10: [''], // houston
                    11: [''], // dallas
                    12: [''], // sf
                    13: [''], // la
                    14: [''], // paris
                    15: [''], // austin
                    16: [''], // denver
                    17: [''], // edmonton
                    18: [''], // amsterdam
                    19: [''], // seattle
                    20: [''], // portland
                    21: [''], // san diego
                    22: [''], // atlanta
                    23: [''], // baltimore 
                    24: [''], // berlin
                    25: [''], // boston 
                    26: [''], // brussels
                    27: [''], // charlotte
                    28: [''], // frankfurt
                    29: [''], // halifax
                    30: [''], // indianapolis
                    31: [''], // kitchenerwaterloo
                    32: [''], // london
                    33: [''], // minneapolis
                    34: [''], // nantes
                    35: [''], // nashville
                    36: [''], // phoenix
                    37: [''], // pittsburgh
                    38: [''], // sacramento
                    39: [''], // victoria
                    40: [''], // louisville
                    41: [''], // albuquerque
                    42: [''], // cincinnati
                    43: [''], // helena
                    44: [''], // tulsa
                    45: [''], // winnipeg
                    46: [''], // richmond
                    47: [''], // miami
                },
            },
            reg_oxalique: {
                id: 'reg_oxalique',
                name: {
                    en: 'Oxalic acid treatment',
                    fr: 'Oxalique',
                },
                recommendedDateRanges: {
                    0: [""],
                    1: [""],
                    2: [""],
                    3: [""],
                    4: [""],
                    5: [""],
                    6: [""],
                    7: [""],
                    8: [""],
                    9: [""],
                    10: [""],
                    11: [""],
                    12: [""],
                    13: [""],
                    14: [""],
                    15: [""],
                    16: [""],
                    17: [""],
                    18: ["5-7"],
                    19: [""],
                    20: [""],
                    21: [""],
                    22: [""],
                    23: [""],
                    24: [""],
                    25: [""],
                    26: ["5-7"],
                    27: [""],
                    28: [""],
                    29: [""],
                    30: [""],
                    31: [""],
                    32: ["1-2", "4-4"],
                    33: [""],
                    34: [""],
                    35: [""],
                    36: [""],
                    37: [""],
                    38: [""],
                    39: [""],
                    40: [''], // louisville
                    41: [''], // albuquerque
                    42: [''], // cincinnati
                    43: [''], // helena
                    44: [''], // tulsa
                    45: [''], // winnipeg
                    46: [''], // richmond
                    47: [''], // miami
                },
            },

            reg_1: {
                name: { en: "1", fr: "1" },
                id: "reg_1",
                recommendedDateRanges: {
                    0: ["13-15"],
                    1: ["14-16"],
                    2: ["15-17"],
                    3: ["15-17"],
                    4: ["14-16"],
                    5: ["9-11"],
                    6: ["15-17"],
                    7: ["13-15"],
                    8: ["12-14"],
                    9: ["10-12"],
                    10: ["7-9"],
                    11: ["7-9"],
                    12: ["4-6"],
                    13: ["4-6"],
                    14: [""],
                    15: ["7-9"],
                    16: ["15-17"],
                    17: ["14-16"],
                    18: [""],
                    19: ["9-11"],
                    20: ["9-11"],
                    21: ["4-6"],
                    22: ["6-8"], // atlanta
                    23: [""],
                    24: ["9-11"],
                    25: ["14-16"],
                    26: [""],
                    27: [""],
                    28: ["9-11"],
                    29: ["14-16"],
                    30: [""],
                    31: ["15-17"],
                    32: [""],
                    33: ["15-17"],
                    34: [""],
                    35: ["7-9"], // nashville
                    36: ["4-6"],
                    37: ["13-15"],
                    38: ["4-6"],
                    39: ["9-11"],
                    40: ["13-15"], // louisville
                    41: ["13-15"], // albuquerque
                    42: ["13-15"], // cincinnati
                    43: ["13-15"], // helena
                    44: ["19-21"], // tulsa
                    45: ["19-21"], // winnipeg
                    46: ['10-12'], // richmond
                    47: [''], // miami
                },
            },
            reg_2: {
                name: { en: "2", fr: "2" },
                id: "reg_2",
                recommendedDateRanges: {
                    0: ["18-20"],
                    1: ["17-19"],
                    2: ["18-20"],
                    3: ["19-21"],
                    4: ["17-19"],
                    5: ["12-14"],
                    6: ["18-20"],
                    7: ["17-19"],
                    8: ["16-18"],
                    9: ["13-15"],
                    10: ["10-12"],
                    11: ["10-12"],
                    12: ["7-8"],
                    13: ["7-9"],
                    14: ["10-12"],
                    15: ["10-12"],
                    16: ["18-20"],
                    17: ["17-19"],
                    18: [""],
                    19: ["12-14"],
                    20: ["12-14"],
                    21: ["7-9"],
                    22: ["9-11"],
                    23: [""],
                    24: ["12-14"],
                    25: ["18-20"],
                    26: ["9-11"],
                    27: ["13-15"],
                    28: ["12-14"],
                    29: ["19-21"],
                    30: [""],
                    31: ["18-20"],
                    32: ["9-11"],
                    33: ["18-20"],
                    34: ["10-12"],
                    35: ["10-12"], // nashville
                    36: ["7-9"],
                    37: ["16-18"],
                    38: ["7-9"],
                    39: ["12-14"],
                    40: ['16-18'], // louisville
                    41: ['16-18'], // albuquerque
                    42: ['16-18'], // cincinnati
                    43: ['16-18'], // helena
                    44: ['22-24'], // tulsa
                    45: ['22-24'], // winnipeg
                    46: ['13-15'], // richmond
                    47: [''], // miami
                },
            },
            reg_3: {
                name: { en: "3", fr: "3" },
                id: "reg_3",
                recommendedDateRanges: {
                    0: ["22-24"],
                    1: ["20-22"],
                    2: ["22-24"],
                    3: ["22-24"],
                    4: ["20-22"],
                    5: ["16-18"],
                    6: ["21-23"],
                    7: ["20-22"],
                    8: ["19-21"],
                    9: ["17-19"],
                    10: ["13-15"],
                    11: ["13-15"],
                    12: ["9-11"],
                    13: ["10-12"],
                    14: ["14-16"],
                    15: ["13-15"],
                    16: ["21-23"],
                    17: ["20-22"],
                    18: [""],
                    19: ["16-18"],
                    20: ["15-17"],
                    21: ["10-12"],
                    22: ["12-14"],
                    23: [""],
                    24: ["15-17"],
                    25: ["21-23"],
                    26: ["12-14"],
                    27: ["16-18"],
                    28: ["15-17"],
                    29: ["22-24"],
                    30: [""],
                    31: ["22-24"],
                    32: ["12-14"],
                    33: ["21-23"],
                    34: ["14-16"],
                    35: ["13-15"], // nashville
                    36: ["10-12"],
                    37: ["19-21"],
                    38: ["10-12"],
                    39: ["15-17"],
                    40: ['19-21'], // louisville
                    41: ['19-21'], // albuquerque
                    42: ['19-21'], // cincinnati
                    43: ['19-21'], // helena
                    44: ['25-27'], // tulsa
                    45: ['25-27'], // winnipeg
                    46: ['17-19'], // richmond
                    47: [''], // miami
                },
            },
            reg_4: {
                name: { en: "4", fr: "4" },
                id: "reg_4",
                recommendedDateRanges: {
                    0: ["25-27"],
                    1: ["24-26"],
                    2: ["25-27"],
                    3: ["25-27"],
                    4: ["23-25"],
                    5: ["19-21"],
                    6: ["24-26"],
                    7: ["23-25"],
                    8: ["22-24"],
                    9: ["20-22"],
                    10: ["17-19"],
                    11: ["17-19"],
                    12: ["12-14"],
                    13: ["13-15"],
                    14: ["17-19"],
                    15: ["17-19"],
                    16: ["24-26"],
                    17: ["23-25"],
                    18: ["15-17"],
                    19: ["19-21"],
                    20: ["18-20"],
                    21: ["13-15"],
                    22: ["15-17"],
                    23: [""],
                    24: ["18-20"],
                    25: ["24-26"],
                    26: ["15-17"],
                    27: ["19-21"],
                    28: ["18-20"],
                    29: ["25-27"],
                    30: [""],
                    31: ["25-27"],
                    32: ["15-17"],
                    33: ["24-26"],
                    34: ["17-19"],
                    35: ["17-19"], // nashville
                    36: ["13-15"],
                    37: ["22-24"],
                    38: ["13-15"],
                    39: ["18-20"],
                    40: ['22-24'], // louisville
                    41: ['22-24'], // albuquerque
                    42: ['22-24'], // cincinnati
                    43: ['22-24'], // helena
                    44: ['28-30'], // tulsa
                    45: ['28-30'], // winnipeg
                    46: ['20-22'], // richmond
                    47: [''], // miami
                },
            },
            reg_5: {
                name: { en: "5", fr: "5" },
                id: "reg_5",
                recommendedDateRanges: {
                    0: ["28-30"],
                    1: ["27-29"],
                    2: ["28-30"],
                    3: ["28-30"],
                    4: ["26-28"],
                    5: ["22-24"],
                    6: ["27-29"],
                    7: ["26-28"],
                    8: ["25-27"],
                    9: ["24-26"],
                    10: ["20-22"],
                    11: ["20-22"],
                    12: ["18-20"],
                    13: ["16-18"],
                    14: ["20-22"],
                    15: ["20-22"],
                    16: ["27-29"],
                    17: ["26-28"],
                    18: ["18-20"],
                    19: ["22-24"],
                    20: ["21-23"],
                    21: ["16-18"],
                    22: ["18-20"],
                    23: [""],
                    24: ["21-23"],
                    25: ["27-29"],
                    26: ["18-20"],
                    27: ["22-24"],
                    28: ["21-23"],
                    29: ["28-30"],
                    30: [""],
                    31: ["28-30"],
                    32: ["18-20"],
                    33: ["27-29"],
                    34: ["20-22"],
                    35: ["20-22"], // nashville
                    36: ["16-18"],
                    37: ["25-27"],
                    38: ["16-18"],
                    39: ["21-23"],
                    40: ['25-27'], // louisville
                    41: ['25-27'], // albuquerque
                    42: ['25-27'], // cincinnati
                    43: ['25-27'], // helena
                    44: ['31-33'], // tulsa
                    45: ['31-33'], // winnipeg
                    46: ['23-25'], // richmond
                    47: [''], // miami
                },
            },
            reg_6: {
                name: { en: "6", fr: "6" },
                id: "reg_6",
                recommendedDateRanges: {
                    0: ["32-34"],
                    1: ["31-33"],
                    2: ["31-33"],
                    3: ["31-33"],
                    4: ["30-32"],
                    5: ["25-27"],
                    6: ["31-33"],
                    7: ["30-32"],
                    8: ["29-31"],
                    9: ["28-30"],
                    10: ["23-25"],
                    11: ["23-25"],
                    12: ["21-23"],
                    13: ["19-21"],
                    14: ["23-25"],
                    15: ["23-25"],
                    16: ["30-32"],
                    17: ["30-32"],
                    18: ["21-23"],
                    19: ["25-27"],
                    20: ["24-26"],
                    21: ["19-21"],
                    22: ["21-23"],
                    23: [""],
                    24: ["24-26"],
                    25: ["31-33"],
                    26: ["21-23"],
                    27: ["25-27"],
                    28: ["24-26"],
                    29: ["31-33"],
                    30: [""],
                    31: ["31-33"],
                    32: ["21-23"],
                    33: ["31-33"],
                    34: ["23-25"],
                    35: ["23-25"], // nashville 
                    36: ["19-21"],
                    37: ["28-30"],
                    38: ["19-21"],
                    39: ["24-26"],
                    40: ['28-30'], // louisville
                    41: ['28-30'], // albuquerque
                    42: ['28-30'], // cincinnati
                    43: ['28-30'], // helena
                    44: ['34-36'], // tulsa
                    45: ['34-36'], // winnipeg
                    46: ['26-28'], // richmond
                    47: [''], // miami
                },
            },
            reg_7: {
                name: { en: "7", fr: "7" },
                id: "reg_7",
                recommendedDateRanges: {
                    0: ["35-37"],
                    1: ["34-36"],
                    2: ["34-36"],
                    3: ["35-37"],
                    4: ["33-35"],
                    5: ["29-31"],
                    6: ["34-36"],
                    7: ["33-35"],
                    8: ["32-34"],
                    9: ["31-33"],
                    10: ["27-29"],
                    11: ["27-29"],
                    12: ["24-26"],
                    13: ["23-25"],
                    14: ["26-28"],
                    15: ["27-29"],
                    16: ["34-36"],
                    17: ["33-35"],
                    18: ["24-26"],
                    19: ["29-31"],
                    20: ["27-29"],
                    21: ["23-25"],
                    22: ["24-26"],
                    23: [""],
                    24: ["28-30"],
                    25: ["34-36"],
                    26: ["24-26"],
                    27: ["28-30"],
                    28: ["28-30"],
                    29: ["34-36"],
                    30: [""],
                    31: ["34-36"],
                    32: ["24-26"],
                    33: ["34-36"],
                    34: ["26-28"],
                    35: ["27-29"], // nashville 
                    36: ["23-25"],
                    37: ["31-33"],
                    38: ["22-24"],
                    39: ["27-29"],
                    40: ['31-33'], // louisville
                    41: ['31-33'], // albuquerque
                    42: ['31-33'], // cincinnati
                    43: ['31-33'], // helena
                    44: ['37-39'], // tulsa
                    45: ['37-39'], // winnipeg
                    46: ['29-31'], // richmond
                    47: [''], // miami
                },
            },
            reg_8: {
                name: { en: "8", fr: "8" },
                id: "reg_8",
                recommendedDateRanges: {
                    0: ["39-41"],
                    1: ["37-39"],
                    2: ["38-40"],
                    3: ["39-41"],
                    4: ["37-39"],
                    5: ["33-35"],
                    6: ["37-39"],
                    7: ["37-39"],
                    8: ["36-39"],
                    9: ["34-36"],
                    10: ["30-32"],
                    11: ["30-32"],
                    12: ["27-29"],
                    13: ["27-29"],
                    14: ["29-31"],
                    15: ["30-32"],
                    16: ["38-40"],
                    17: ["37-39"],
                    18: ["28-30"],
                    19: ["33-35"],
                    20: ["30-32"],
                    21: ["27-29"],
                    22: ["27-29"],
                    23: [""],
                    24: ["32-34"],
                    25: ["38-40"],
                    26: ["28-30"],
                    27: ["31-33"],
                    28: ["32-34"],
                    29: ["37-39"],
                    30: [""],
                    31: ["38-40"],
                    32: ["27-29"],
                    33: ["38-40"],
                    34: ["29-31"],
                    35: ["31-33"], // nashville
                    36: ["27-29"],
                    37: ["34-36"],
                    38: ["25-27"],
                    39: ["30-32"],
                    40: ['34-36'], // louisville
                    41: ['34-36'], // albuquerque
                    42: ['34-36'], // cincinnati
                    43: ['34-36'], // helena
                    44: ['40-42'], // tulsa
                    45: ['40-42'], // winnipeg
                    46: ['32-34'], // richmond
                    47: [''], // miami
                },
            },
            reg_9: {
                name: { en: "9", fr: "9" },
                id: "reg_9",
                recommendedDateRanges: {
                    0: ["42-44"],
                    1: ["41-43"],
                    2: ["42-44"],
                    3: ["42-44"],
                    4: ["40-42"],
                    5: ["36-38"],
                    6: ["41-43"],
                    7: ["40-42"],
                    8: ["39-41"],
                    9: ["37-39"],
                    10: ["33-35"],
                    11: ["33-35"],
                    12: ["31-33"],
                    13: ["30-32"],
                    14: ["32-34"],
                    15: ["33-35"],
                    16: ["42-44"],
                    17: ["40-42"],
                    18: ["32-34"],
                    19: ["36-38"],
                    20: ["33-35"],
                    21: ["30-32"],
                    22: ["30-32"],
                    23: [""],
                    24: ["35-37"],
                    25: ["41-43"],
                    26: ["32-34"],
                    27: ["34-36"],
                    28: ["35-37"],
                    29: ["40-42"],
                    30: [""],
                    31: ["42-44"],
                    32: ["30-32"],
                    33: ["41-43"],
                    34: ["32-34"],
                    35: ["34-36"], // nashville
                    36: ["30-32"],
                    37: ["37-39"],
                    38: ["28-30"],
                    39: ["33-35"],
                    40: ['37-39'], // louisville
                    41: ['37-39'], // albuquerque
                    42: ['37-39'], // cincinnati
                    43: ['37-39'], // helena
                    44: ['43-45'], // tulsa
                    45: ['43-45'], // winnipeg
                    46: ['35-37'], // richmond
                    47: [''], // miami
                },
            },
            reg_10: {
                name: { en: "10", fr: "10" },
                id: "reg_10",
                recommendedDateRanges: {
                    0: [""],
                    1: [""],
                    2: [""],
                    3: [""],
                    4: [""],
                    5: ["40-42"],
                    6: ["45-47"],
                    7: ["43-45"],
                    8: ["42-44"],
                    9: ["41-43"],
                    10: ["36-38"],
                    11: ["36-38"],
                    12: ["34-36"],
                    13: ["34-36"],
                    14: ["35-37"],
                    15: ["36-38"],
                    16: [""],
                    17: [""],
                    18: ["35-37"],
                    19: ["40-42"],
                    20: ["36-38"],
                    21: ["34-36"],
                    22: ["33-35"],
                    23: [""],
                    24: ["38-40"],
                    25: ["44-46"],
                    26: ["35-37"],
                    27: ["38-40"],
                    28: ["38-40"],
                    29: ["43-45"],
                    30: [""],
                    31: [""],
                    32: ["35-37"],
                    33: [""],
                    34: ["35-37"],
                    35: ["38-40"], // nashville
                    36: ["34-36"],
                    37: ["40-42"],
                    38: ["31-33"],
                    39: ["36-38"],
                    40: ['40-42'], // louisville
                    41: ['40-42'], // albuquerque
                    42: ['40-42'], // cincinnati
                    43: ['40-42'], // helena
                    44: ['46-49'], // tulsa
                    45: ['46-49'], // winnipeg
                    46: ['38-40'], // richmond
                    47: [''], // miami
                },
            },
            reg_11: {
                name: { en: "11", fr: "11" },
                id: "reg_11",
                recommendedDateRanges: {
                    0: [""],
                    1: [""],
                    2: [""],
                    3: [""],
                    4: [""],
                    5: ["47-49"],
                    6: [""],
                    7: [""],
                    8: [""],
                    9: [""],
                    10: ["40-42"],
                    11: ["40-42"],
                    12: ["37-39"],
                    13: ["38-40"],
                    14: ["38-40"],
                    15: ["40-42"],
                    16: [""],
                    17: [""],
                    18: ["38-40"],
                    19: ["47-49"],
                    20: ["39-41"],
                    21: ["38-40"],
                    22: ["36-38"],
                    23: [""],
                    24: ["41-43"],
                    25: [""],
                    26: ["38-40"],
                    27: ["40-42"],
                    28: ["41-43"],
                    29: ["47-49"],
                    30: [""],
                    31: [""],
                    32: ["38-40"],
                    33: [""],
                    34: ["38-40"],
                    35: [""], // nashville
                    36: ["38-40"],
                    37: ["43-45"],
                    38: ["34-36"],
                    39: ["39-41"],
                    40: ['43-45'], // louisville
                    41: ['43-45'], // albuquerque
                    42: ['43-45'], // cincinnati
                    43: ['43-45'], // helena
                    44: ['50-52'], // tulsa
                    45: ['50-52'], // winnipeg
                    46: ['41-43'], // richmond
                    47: [''], // miami
                },
            },
            reg_12: {
                name: { en: "12", fr: "12" },
                id: "reg_12",
                recommendedDateRanges: {
                    0: [""],
                    1: [""],
                    2: [""],
                    3: [""],
                    4: [""],
                    5: [""],
                    6: [""],
                    7: [""],
                    8: [""],
                    9: [""],
                    10: ["44-46"],
                    11: ["44-46"],
                    12: ["40-42"],
                    13: ["41-43"],
                    14: ["41-43"],
                    15: ["44-46"],
                    16: [""],
                    17: [""],
                    18: ["41-43"],
                    19: [""],
                    20: ["42-44"],
                    21: ["41-43"],
                    22: ["39-41"],
                    23: [""],
                    24: ["44-46"],
                    25: [""],
                    26: ["41-43"],
                    27: ["43-45"],
                    28: ["44-46"],
                    29: [""],
                    30: [""],
                    31: [""],
                    32: ["41-43"],
                    33: [""],
                    34: ["41-43"],
                    35: [""], // nashville
                    36: ["41-43"],
                    37: ["46-49"],
                    38: ["37-39"],
                    39: ["42-44"],
                    40: ['46-49'], // louisville
                    41: ['46-49'], // albuquerque
                    42: ['46-49'], // cincinnati
                    43: ['46-49'], // helena
                    44: [''], // tulsa
                    45: [''], // winnipeg
                    46: ['44-46'], // richmond
                    47: [''], // miami
                },
            },
            reg_13: {
                name: { en: "13", fr: "13" },
                id: "reg_13",
                recommendedDateRanges: {
                    0: [""],
                    1: [""],
                    2: [""],
                    3: [""],
                    4: [""],
                    5: [""],
                    6: [""],
                    7: [""],
                    8: [""],
                    9: [""],
                    10: [""],
                    11: [""],
                    12: [""],
                    13: [""],
                    14: ["44-46"],
                    15: [""],
                    16: [""],
                    17: [""],
                    18: ["44-46"],
                    19: [""],
                    20: ["45-47"],
                    21: [""],
                    22: ["42-44"],
                    23: [""],
                    24: ["50-52"],
                    25: [""],
                    26: ["44-46"],
                    27: ["46-49"],
                    28: ["50-52"],
                    29: [""],
                    30: [""],
                    31: [""],
                    32: ["44-46"],
                    33: [""],
                    34: ["44-46"],
                    35: [""],
                    36: [""],
                    37: ["50-52"],
                    38: ["40-42"],
                    39: ["45-47"],
                    40: ['50-52'], // louisville
                    41: [''], // albuquerque
                    42: ['50-52'], // cincinnati
                    43: ['50-52'], // helena
                    44: [''], // tulsa
                    45: [''], // winnipeg
                    46: ['47-49'], // richmond
                    47: [''], // miami
                },
            },
            reg_14: {
                name: { en: "14", fr: "14" },
                id: "reg_14",
                recommendedDateRanges: {
                    0: [""],
                    1: [""],
                    2: [""],
                    3: [""],
                    4: [""],
                    5: [""],
                    6: [""],
                    7: [""],
                    8: [""],
                    9: [""],
                    10: [""],
                    11: [""],
                    12: [""],
                    13: [""],
                    14: ["50-52"],
                    15: [""],
                    16: [""],
                    17: [""],
                    18: ["50-52"],
                    19: [""],
                    20: [""],
                    21: [""],
                    22: [""],
                    23: [""],
                    24: [""],
                    25: [""],
                    26: ["50-52"],
                    27: [""],
                    28: [""],
                    29: [""],
                    30: [""],
                    31: [""],
                    32: ["50-52"],
                    33: [""],
                    34: ["50-52"],
                    35: [""],
                    36: [""],
                    37: [""],
                    38: ['43-45'],
                    39: [""],
                    40: [''], // louisville
                    41: [''], // albuquerque
                    42: [''], // cincinnati
                    43: [''], // helena
                    44: [''], // tulsa
                    45: [''], // winnipeg
                    46: [''], // richmond
                    47: [''], // miami
                },
            },
            reg_15: {
                id: 'reg_15',
                name: {
                    en: '15',
                    fr: '15',
                },
                recommendedDateRanges: {
                    0: [''], // montreal
                    1: [''], // quebec
                    2: [''], // toronto
                    3: [''], // ottawa
                    4: [''], // calgary
                    5: [''], // vancouver
                    6: [''], // chicago
                    7: [''], // nyc
                    8: [''], // philly
                    9: [''], // dc
                    10: [''], // houston
                    11: [''], // dallas
                    12: [''], // sf
                    13: [''], // la
                    14: [''], // paris
                    15: [''], // austin
                    16: [''], // denver
                    17: [''], // edmonton
                    18: [''], // amsterdam
                    19: [''], // seattle
                    20: [''], // portland
                    21: [''], // san diego
                    22: [''], // atlanta
                    23: [''], // baltimore 
                    24: [''], // berlin
                    25: [''], // boston 
                    26: [''], // brussels
                    27: [''], // charlotte
                    28: [''], // frankfurt
                    29: [''], // halifax
                    30: [''], // indianapolis
                    31: [''], // kitchenerwaterloo
                    32: [''], // london
                    33: [''], // minneapolis
                    34: [''], // nantes
                    35: [''], // nashville
                    36: [''], // phoenix
                    37: [''], // pittsburgh
                    38: ['46-49'], // sacramento
                    39: [''], // victoria
                    40: [''], // louisville
                    41: [''], // albuquerque
                    42: [''], // cincinnati
                    43: [''], // helena
                    44: [''], // tulsa
                    45: [''], // winnipeg
                    46: [''], // richmond
                    47: [''], // miami
                },
            },
            ate_introduction: {
                availableIn: ['Canada', 'United States', 'Europe'],
                id: 'ate_introduction',
                name: {
                    en: 'Discover the World of Bees',
                    fr: 'À la découverte du monde des abeilles',
                },
                recommendedDateRanges: {
                    0: [''], // montreal
                    1: [''], // quebec
                    2: [''], // toronto
                    3: [''], // ottawa
                    4: [''], // calgary
                    5: [''], // vancouver
                    6: [''], // chicago
                    7: [''], // nyc
                    8: [''], // philly
                    9: [''], // dc
                    10: [''], // houston
                    11: [''], // dallas
                    12: [''], // sf
                    13: [''], // la
                    14: [''], // paris
                    15: [''], // austin
                    16: [''], // denver
                    17: [''], // edmonton
                    18: [''], // amsterdam
                    19: [''], // seattle
                    20: [''], // portland
                    21: [''], // san diego
                    22: [''], // atlanta
                    23: [''], // baltimore 
                    24: [''], // berlin
                    25: [''], // boston 
                    26: [''], // brussels
                    27: [''], // charlotte
                    28: [''], // frankfurt
                    29: [''], // halifax
                    30: [''], // indianapolis
                    31: [''], // kitchenerwaterloo
                    32: [''], // london
                    33: [''], // minneapolis
                    34: [''], // nantes
                    35: [''], // nashville
                    36: [''], // phoenix
                    37: [''], // pittsburgh
                    38: [''], // sacramento
                    39: [''], // victoria
                    40: [''], // louisville
                    41: [''], // albuquerque
                    42: [''], // cincinnati
                    43: [''], // helena
                    44: [''], // tulsa
                    45: [''], // winnipeg
                    46: [''], // richmond
                    47: [''], // miami
                },
                tags: [
                    "LARGE_GROUP",
                    "SMALL_GROUP",
                    "INTRODUCTORY",
                    "INDOOR",
                    "PRESENTATION",
                    "IN_PERSON",
                    "ONLINE",
                    "ALL_YEAR",
                ],
                timings: {
                    setup: 30,
                    show: 60,
                    cleanup: 30,
                    total: 120,
                },
                virtual: true,
                inPerson: true,
            },
            ate_beeswax: {
                availableIn: ['Canada', 'United States', 'Europe'],
                id: 'ate_beeswax',
                name: {
                    en: 'The Wonders of Beeswax',
                    fr: 'Découvrir la cire d’abeille',
                },
                recommendedDateRanges: {
                    0: [''], // montreal
                    1: [''], // quebec
                    2: [''], // toronto
                    3: [''], // ottawa
                    4: [''], // calgary
                    5: [''], // vancouver
                    6: [''], // chicago
                    7: [''], // nyc
                    8: [''], // philly
                    9: [''], // dc
                    10: [''], // houston
                    11: [''], // dallas
                    12: [''], // sf
                    13: [''], // la
                    14: [''], // paris
                    15: [''], // austin
                    16: [''], // denver
                    17: [''], // edmonton
                    18: [''], // amsterdam
                    19: [''], // seattle
                    20: [''], // portland
                    21: [''], // san diego
                    22: [''], // atlanta
                    23: [''], // baltimore 
                    24: [''], // berlin
                    25: [''], // boston 
                    26: [''], // brussels
                    27: [''], // charlotte
                    28: [''], // frankfurt
                    29: [''], // halifax
                    30: [''], // indianapolis
                    31: [''], // kitchenerwaterloo
                    32: [''], // london
                    33: [''], // minneapolis
                    34: [''], // nantes
                    35: [''], // nashville
                    36: [''], // phoenix
                    37: [''], // pittsburgh
                    38: [''], // sacramento
                    39: [''], // victoria
                    40: [''], // louisville
                    41: [''], // albuquerque
                    42: [''], // cincinnati
                    43: [''], // helena
                    44: [''], // tulsa
                    45: [''], // winnipeg
                    46: [''], // richmond
                    47: [''], // miami
                },
                tags: [
                    "SMALL_GROUP",
                    "INDOOR",
                    "IN_PERSON",
                    "ALL_YEAR",
                    "DIG_DEEPER",
                    "ACTIVITY",
                    "OUTDOOR",
                    "HIVE_PRODUCTS",
                    "TEAM_BUILDING",
                    "HANDS_ON",
                ],
                timings: {
                    setup: 45,
                    show: 60,
                    cleanup: 30,
                    total: 120,
                },
                participantsLimit: {
                    inPerson: 30,
                },
                inPerson: true,
            },
            ate_extraction: {
                availableIn: ['Canada', 'United States', 'Europe'],
                id: 'ate_extraction',
                name: {
                    en: 'From Hive to Honey Jar',
                    fr: 'De la ruche à la récolte',
                },
                recommendedDateRanges: {
                    0: [''], // montreal
                    1: [''], // quebec
                    2: [''], // toronto
                    3: [''], // ottawa
                    4: [''], // calgary
                    5: [''], // vancouver
                    6: [''], // chicago
                    7: [''], // nyc
                    8: [''], // philly
                    9: [''], // dc
                    10: [''], // houston
                    11: [''], // dallas
                    12: [''], // sf
                    13: [''], // la
                    14: [''], // paris
                    15: [''], // austin
                    16: [''], // denver
                    17: [''], // edmonton
                    18: [''], // amsterdam
                    19: [''], // seattle
                    20: [''], // portland
                    21: [''], // san diego
                    22: [''], // atlanta
                    23: [''], // baltimore 
                    24: [''], // berlin
                    25: [''], // boston 
                    26: [''], // brussels
                    27: [''], // charlotte
                    28: [''], // frankfurt
                    29: [''], // halifax
                    30: [''], // indianapolis
                    31: [''], // kitchenerwaterloo
                    32: [''], // london
                    33: [''], // minneapolis
                    34: [''], // nantes
                    35: [''], // nashville
                    36: [''], // phoenix
                    37: [''], // pittsburgh
                    38: [''], // sacramento
                    39: [''], // victoria
                    40: [''], // louisville
                    41: [''], // albuquerque
                    42: [''], // cincinnati
                    43: [''], // helena
                    44: [''], // tulsa
                    45: [''], // winnipeg
                    46: [''], // richmond
                    47: [''], // miami
                },
                tags: [
                    "SMALL_GROUP",
                    "INTRODUCTORY",
                    "INDOOR",
                    "IN_PERSON",
                    "ONLINE",
                    "ACTIVITY",
                    "HIVE_PRODUCTS",
                    "TEAM_BUILDING",
                    "HANDS_ON",
                    "SEASONAL",
                ],
                timings: {
                    setup: 30,
                    show: 60,
                    cleanup: 45,
                    total: 135,
                },
                participantsLimit: {
                    inPerson: 30,
                },
                dailyLimit: 2,
                virtual: true,
                inPerson: true,
            },
            ate_kiosque: {
                availableIn: ['Canada', 'United States', 'Europe'],
                id: 'ate_kiosque',
                name: {
                    en: "Kiosk: What's The Buzz?",
                    fr: "Kiosque: c'est quoi ce buzz?",
                },
                recommendedDateRanges: {
                    0: [''], // montreal
                    1: [''], // quebec
                    2: [''], // toronto
                    3: [''], // ottawa
                    4: [''], // calgary
                    5: [''], // vancouver
                    6: [''], // chicago
                    7: [''], // nyc
                    8: [''], // philly
                    9: [''], // dc
                    10: [''], // houston
                    11: [''], // dallas
                    12: [''], // sf
                    13: [''], // la
                    14: [''], // paris
                    15: [''], // austin
                    16: [''], // denver
                    17: [''], // edmonton
                    18: [''], // amsterdam
                    19: [''], // seattle
                    20: [''], // portland
                    21: [''], // san diego
                    22: [''], // atlanta
                    23: [''], // baltimore 
                    24: [''], // berlin
                    25: [''], // boston 
                    26: [''], // brussels
                    27: [''], // charlotte
                    28: [''], // frankfurt
                    29: [''], // halifax
                    30: [''], // indianapolis
                    31: [''], // kitchenerwaterloo
                    32: [''], // london
                    33: [''], // minneapolis
                    34: [''], // nantes
                    35: [''], // nashville
                    36: [''], // phoenix
                    37: [''], // pittsburgh
                    38: [''], // sacramento
                    39: [''], // victoria
                    40: [''], // louisville
                    41: [''], // albuquerque
                    42: [''], // cincinnati
                    43: [''], // helena
                    44: [''], // tulsa
                    45: [''], // winnipeg
                    46: [''], // richmond
                    47: [''], // miami
                },
                tags: [
                    "LARGE_GROUP",
                    "INTRODUCTORY",
                    "INDOOR",
                    "IN_PERSON",
                    "ALL_YEAR",
                    "OUTDOOR",
                    "CONVERSATION_BASED",
                    "KIOSK",
                ],
                timings: {
                    setup: 30,
                    show: 60,
                    cleanup: 30,
                    total: 120,
                },
                inPerson: true,
            },
            ate_ouverture: {
                availableIn: ['Canada', 'United States', 'Europe'],
                id: 'ate_ouverture',
                name: {
                    en: 'Meet Your Bees',
                    fr: 'Rencontrez vos abeilles',
                },
                recommendedDateRanges: {
                    0: [''], // montreal
                    1: [''], // quebec
                    2: [''], // toronto
                    3: [''], // ottawa
                    4: [''], // calgary
                    5: [''], // vancouver
                    6: [''], // chicago
                    7: [''], // nyc
                    8: [''], // philly
                    9: [''], // dc
                    10: [''], // houston
                    11: [''], // dallas
                    12: [''], // sf
                    13: [''], // la
                    14: [''], // paris
                    15: [''], // austin
                    16: [''], // denver
                    17: [''], // edmonton
                    18: [''], // amsterdam
                    19: [''], // seattle
                    20: [''], // portland
                    21: [''], // san diego
                    22: [''], // atlanta
                    23: [''], // baltimore 
                    24: [''], // berlin
                    25: [''], // boston 
                    26: [''], // brussels
                    27: [''], // charlotte
                    28: [''], // frankfurt
                    29: [''], // halifax
                    30: [''], // indianapolis
                    31: [''], // kitchenerwaterloo
                    32: [''], // london
                    33: [''], // minneapolis
                    34: [''], // nantes
                    35: [''], // nashville
                    36: [''], // phoenix
                    37: [''], // pittsburgh
                    38: [''], // sacramento
                    39: [''], // victoria
                    40: [''], // louisville
                    41: [''], // albuquerque
                    42: [''], // cincinnati
                    43: [''], // helena
                    44: [''], // tulsa
                    45: [''], // winnipeg
                    46: [''], // richmond
                    47: [''], // miami
                },
                tags: [
                    "SMALL_GROUP",
                    "INTRODUCTORY",
                    "INDOOR",
                    "IN_PERSON",
                    "ONLINE",
                    "OUTDOOR",
                    "TEAM_BUILDING",
                    "SEASONAL",
                    "AT_THE_HIVE",
                ],
                timings: {
                    setup: 30,
                    show: 60,
                    cleanup: 30,
                    total: 120,
                },
                participantsLimit: {
                    inPerson: 30,
                },
                virtual: true,
                inPerson: true,
            },
            ate_beekeeper: {
                availableIn: ['Canada', 'United States', 'Europe'],
                id: 'ate_beekeeper',
                name: {
                    en: 'Meet Your Beekeeper',
                    fr: 'Rencontrez votre apiculteur ou apicultrice',
                },
                recommendedDateRanges: {
                    0: [''], // montreal
                    1: [''], // quebec
                    2: [''], // toronto
                    3: [''], // ottawa
                    4: [''], // calgary
                    5: [''], // vancouver
                    6: [''], // chicago
                    7: [''], // nyc
                    8: [''], // philly
                    9: [''], // dc
                    10: [''], // houston
                    11: [''], // dallas
                    12: [''], // sf
                    13: [''], // la
                    14: [''], // paris
                    15: [''], // austin
                    16: [''], // denver
                    17: [''], // edmonton
                    18: [''], // amsterdam
                    19: [''], // seattle
                    20: [''], // portland
                    21: [''], // san diego
                    22: [''], // atlanta
                    23: [''], // baltimore 
                    24: [''], // berlin
                    25: [''], // boston 
                    26: [''], // brussels
                    27: [''], // charlotte
                    28: [''], // frankfurt
                    29: [''], // halifax
                    30: [''], // indianapolis
                    31: [''], // kitchenerwaterloo
                    32: [''], // london
                    33: [''], // minneapolis
                    34: [''], // nantes
                    35: [''], // nashville
                    36: [''], // phoenix
                    37: [''], // pittsburgh
                    38: [''], // sacramento
                    39: [''], // victoria
                    40: [''], // louisville
                    41: [''], // albuquerque
                    42: [''], // cincinnati
                    43: [''], // helena
                    44: [''], // tulsa
                    45: [''], // winnipeg
                    46: [''], // richmond
                    47: [''], // miami
                },
                tags: [
                    "LARGE_GROUP",
                    "SMALL_GROUP",
                    "INTRODUCTORY",
                    "INDOOR",
                    "ONLINE",
                    "ALL_YEAR",
                    "CONVERSATION_BASED",
                ],
                timings: {
                    setup: 30,
                    show: 60,
                    cleanup: 30,
                    total: 120,
                },
                virtual: true,
                inPerson: true,
            },
            ate_beethere: {
                availableIn: ['Canada', 'United States'],
                id: 'ate_beethere',
                name: {
                    en: "Bee There or Bee Square: Team Trivia!",
                    fr: "Ça pique la curiosité: quiz en équipe!",
                },
                recommendedDateRanges: {
                    0: [''], // montreal
                    1: [''], // quebec
                    2: [''], // toronto
                    3: [''], // ottawa
                    4: [''], // calgary
                    5: [''], // vancouver
                    6: [''], // chicago
                    7: [''], // nyc
                    8: [''], // philly
                    9: [''], // dc
                    10: [''], // houston
                    11: [''], // dallas
                    12: [''], // sf
                    13: [''], // la
                    14: [''], // paris
                    15: [''], // austin
                    16: [''], // denver
                    17: [''], // edmonton
                    18: [''], // amsterdam
                    19: [''], // seattle
                    20: [''], // portland
                    21: [''], // san diego
                    22: [''], // atlanta
                    23: [''], // baltimore 
                    24: [''], // berlin
                    25: [''], // boston 
                    26: [''], // brussels
                    27: [''], // charlotte
                    28: [''], // frankfurt
                    29: [''], // halifax
                    30: [''], // indianapolis
                    31: [''], // kitchenerwaterloo
                    32: [''], // london
                    33: [''], // minneapolis
                    34: [''], // nantes
                    35: [''], // nashville
                    36: [''], // phoenix
                    37: [''], // pittsburgh
                    38: [''], // sacramento
                    39: [''], // victoria
                    40: [''], // louisville
                    41: [''], // albuquerque
                    42: [''], // cincinnati
                    43: [''], // helena
                    44: [''], // tulsa
                    45: [''], // winnipeg
                    46: [''], // richmond
                    47: [''], // miami
                },
                tags: [
                    "SMALL_GROUP",
                    "INDOOR",
                    "ONLINE",
                    "ALL_YEAR",
                    "DIG_DEEPER",
                    "ACTIVITY",
                    "TEAM_BUILDING",
                ],
                timings: {
                    setup: 30,
                    show: 60,
                    cleanup: 30,
                    total: 120,
                },
                participantsLimit: {
                    virtual: 100,
                    inPerson: 30,
                },
                virtual: true,
                inPerson: false,
            },
            ate_beethemovement: {
                availableIn: ['Canada', 'United States'],
                id: 'ate_beethemovement',
                name: {
                    en: "Bee the Movement",
                    fr: "L'envol de l'agriculture urbaine",
                },
                recommendedDateRanges: {
                    0: [''], // montreal
                    1: [''], // quebec
                    2: [''], // toronto
                    3: [''], // ottawa
                    4: [''], // calgary
                    5: [''], // vancouver
                    6: [''], // chicago
                    7: [''], // nyc
                    8: [''], // philly
                    9: [''], // dc
                    10: [''], // houston
                    11: [''], // dallas
                    12: [''], // sf
                    13: [''], // la
                    14: [''], // paris
                    15: [''], // austin
                    16: [''], // denver
                    17: [''], // edmonton
                    18: [''], // amsterdam
                    19: [''], // seattle
                    20: [''], // portland
                    21: [''], // san diego
                    22: [''], // atlanta
                    23: [''], // baltimore 
                    24: [''], // berlin
                    25: [''], // boston 
                    26: [''], // brussels
                    27: [''], // charlotte
                    28: [''], // frankfurt
                    29: [''], // halifax
                    30: [''], // indianapolis
                    31: [''], // kitchenerwaterloo
                    32: [''], // london
                    33: [''], // minneapolis
                    34: [''], // nantes
                    35: [''], // nashville
                    36: [''], // phoenix
                    37: [''], // pittsburgh
                    38: [''], // sacramento
                    39: [''], // victoria
                    40: [''], // louisville
                    41: [''], // albuquerque
                    42: [''], // cincinnati
                    43: [''], // helena
                    44: [''], // tulsa
                    45: [''], // winnipeg
                    46: [''], // richmond
                    47: [''], // miami
                },
                tags: [
                    "LARGE_GROUP",
                    "SMALL_GROUP",
                    "INDOOR",
                    "PRESENTATION",
                    "IN_PERSON",
                    "ONLINE",
                    "ALL_YEAR",
                    "DIG_DEEPER",
                    "URBAN_ENVIRONMENT",
                ],
                timings: {
                    setup: 30,
                    show: 60,
                    cleanup: 30,
                    total: 120,
                },
                virtual: true,
                inPerson: true,
            },
            ate_explorewildbees: {
                availableIn: ['Canada', 'United States', 'Europe'],
                id: 'ate_explorewildbees',
                name: {
                    en: "Explore the World of Wild Bees",
                    fr: "Explorer le monde des abeilles sauvages",
                },
                recommendedDateRanges: {
                    0: [''], // montreal
                    1: [''], // quebec
                    2: [''], // toronto
                    3: [''], // ottawa
                    4: [''], // calgary
                    5: [''], // vancouver
                    6: [''], // chicago
                    7: [''], // nyc
                    8: [''], // philly
                    9: [''], // dc
                    10: [''], // houston
                    11: [''], // dallas
                    12: [''], // sf
                    13: [''], // la
                    14: [''], // paris
                    15: [''], // austin
                    16: [''], // denver
                    17: [''], // edmonton
                    18: [''], // amsterdam
                    19: [''], // seattle
                    20: [''], // portland
                    21: [''], // san diego
                    22: [''], // atlanta
                    23: [''], // baltimore 
                    24: [''], // berlin
                    25: [''], // boston 
                    26: [''], // brussels
                    27: [''], // charlotte
                    28: [''], // frankfurt
                    29: [''], // halifax
                    30: [''], // indianapolis
                    31: [''], // kitchenerwaterloo
                    32: [''], // london
                    33: [''], // minneapolis
                    34: [''], // nantes
                    35: [''], // nashville
                    36: [''], // phoenix
                    37: [''], // pittsburgh
                    38: [''], // sacramento
                    39: [''], // victoria
                    40: [''], // louisville
                    41: [''], // albuquerque
                    42: [''], // cincinnati
                    43: [''], // helena
                    44: [''], // tulsa
                    45: [''], // winnipeg
                    46: [''], // richmond
                    47: [''], // miami
                },
                tags: [
                    "LARGE_GROUP",
                    "SMALL_GROUP",
                    "PRESENTATION",
                    "IN_PERSON",
                    "ONLINE",
                    "ALL_YEAR",
                    "DIG_DEEPER",
                ],
                timings: {
                    setup: 30,
                    show: 60,
                    cleanup: 30,
                    total: 120,
                },
                virtual: true,
                inPerson: true,
            },
            spe_yard: {
                id: "spe_yard",
                name: {
                    en: "Yard visit",
                    fr: "Yard visit",
                },
            },
            spe_essaimage: {
                id: "spe_essaimage",
                name: {
                    en: "Swarming",
                    fr: "Essaimage",
                },
            },
            spe_diagnostic: {
                id: "spe_diagnostic",
                name: {
                    en: "Disease diagnosis",
                    fr: "Diagnostic de maladie",
                },
            },
            spe_course: {
                id: "spe_course",
                name: {
                    en: "Errand",
                    fr: "Course",
                },
            },
            spe_deplacement: {
                id: "spe_deplacement",
                name: {
                    en: "Moving a hive",
                    fr: "Déplacement de ruche",
                },
            },
            spe_blake: {
                id: "spe_blake",
                name: {
                    en: "See You Later!",
                    fr: "À bientôt!",
                },
            },
        },
        routeDurationExtraMinutes: {
            0: 12,
            1: 12,
            2: 17,
            3: 12,
            4: 14,
            5: 14,
            6: 14,
            7: 14,
            8: 14,
            9: 14,
            10: 14,
            11: 14,
            12: 14,
            13: 14,
            14: 14,
            15: 14,
            16: 14,
            17: 14,
            18: 14, 
            19: 14, 
            20: 14, 
            21: 14, 
        },
        routeDurationMultipliers: {
            0: 1.1,
            1: 1.1,
            2: 1.0,
            3: 1.1,
            4: 1.1,
            5: 1.1,
            6: 1.1,
            7: 1.1,
            8: 1.1,
            9: 1.1,
            10: 1.1,
            11: 1.1,
            12: 1.1,
            13: 1.1,
            14: 1.1,
            15: 1.1,
            16: 1.1,
            17: 1.1,
            18: 1.1, 
            19: 1.1, 
            20: 1.1, 
            21: 1.1, 
        },
    };
    var lookup = function(map, type) {
        if (undefined === typeof map.unknown) throw new Error('Expected map to contain an unknown entry.');
        return function(code) {
            if ('number' !== typeof code && 'string' !== typeof code) throw new Error('Expected code to be a string or number.');
            return map[code] || map.unknown;
        };
    };
    var lookupFromParseObject = function(getter, value, name) {
        return function(object) {
            if (object === null || 'object' !== typeof object) throw new Error('Expected ' + name + ' to be an object.');
            if ('function' !== typeof object.get) throw new Error('Expected ' + name + ' to implement get.');
            return getter(object.get(value));
        };
    };

    var proto = {};

    Object.keys(enums).forEach(function(key) {
        proto[key] = function() {
            const all = function() {
                const clonedObject = Object.assign({}, enums[key]);
                delete clonedObject.unknown;
                return clonedObject;
            };
            const get = lookup(enums[key]);

            return {
                "all": all,
                "get": get
            };
        };
    })

    proto.clientRegion = lookupFromParseObject(proto.regions().get, 'Region', 'client');
    proto.statusFromRdv = lookupFromParseObject(proto.rdvStatuses().get, 'Statut', 'RDV');
    proto.recommendedDateRangeByVisitAndRegion = function(visitType, region) {
        var ranges = proto.visitTypes().get(visitType).recommendedDateRanges;
        // default to MTL timings.
        return ranges[region] || ranges[0];
    };
    // backwards compatibility
    proto.region = proto.regions().get;

    return function() {
        return Object.create(proto);
    };
})));
